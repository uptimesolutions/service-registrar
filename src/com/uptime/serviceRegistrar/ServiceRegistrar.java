/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.serviceRegistrar;

import com.uptime.serviceRegistrar.http.client.PeerClient;
import com.uptime.serviceRegistrar.http.listeners.RequestListener;
import com.uptime.serviceRegistrar.threads.MonitorThread;
import com.uptime.serviceRegistrar.threads.PeerSyncThread;
import com.uptime.serviceRegistrar.vo.SubscribeHostVO;
import com.uptime.services.AbstractService;
import com.uptime.services.ServiceConstants;
import static com.uptime.services.ServiceConstants.PEER_REGISTRAR_URL_LIST;
import com.uptime.services.vo.ServiceHostVO;
import static com.uptime.services.util.ServiceUtil.ipAddressCheck;
import static com.uptime.services.util.ServiceUtil.networkCheck;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author ksimmons
 */
public class ServiceRegistrar extends AbstractService {
    public final static String SERVICE_NAME = "Registrar";
    public static String IP_ADDRESS = null;
    public volatile static Map<String,ArrayList<ServiceHostVO>> serviceHosts;
    public volatile static Map<String,ArrayList<SubscribeHostVO>> subscriptions;
    public volatile static List<String> peerUrls;
    private static File serviceHostStateFile = new File("./serviceHost_state.ser");
    private static File subscriptionStateFile = new File("./subscription_state.ser");
    public static Semaphore serviceMutex = new Semaphore(1,true);
    public static Semaphore subscribeMutex = new Semaphore(1,true);
    public static boolean running = true, peerCheck = false, firstRun = true;
    private static boolean peers = false;
    public static int PORT = 0;
    public static RequestListener listener;
    private static MonitorThread monitorThread;
    private static PeerSyncThread peerSyncThread;
    public final static Logger LOGGER = Logger.getLogger(ServiceRegistrar.class.getName());
    public static String currentPeer = null;
    
    
    /**
     * @param args the command line arguments
     * 
     */
    public static void main(String[] args) {
        try{
            // start the logger
            FileHandler fileTxt = new FileHandler("./servicelog.%u.%g.txt",1024*1024,5);
            fileTxt.setFormatter(new SimpleFormatter());
            LOGGER.setUseParentHandlers(false);
            LOGGER.addHandler(fileTxt);
            
            if(args.length == 0){
                LOGGER.log(Level.SEVERE, "Usage: java ServiceMonitor.class port");
                System.exit(1);
            }
            try{
                if((PORT = Integer.parseInt(args[0])) < 1024){
                    LOGGER.log(Level.SEVERE, "Port must be > 1024.");
                    System.exit(1);
                }
            } catch(NumberFormatException e) {
                LOGGER.log(Level.SEVERE, "Port must be an integer.");
                System.exit(1);
            }
            
            // get IP address of local host
            IP_ADDRESS = InetAddress.getLocalHost().getHostAddress();
            
            System.out.println("Service Port: " + PORT);
            System.out.println("Service Ip Address: " + IP_ADDRESS);
            
            startup();
            monitoringThreads();
            shutdown();
        } catch(Exception e){
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            System.exit(1);
        }
    }
    
    /**
     * Startup the service
     */
    private static void startup() throws Exception{
        System.out.println("Startup initiated...");
        LOGGER.log(Level.INFO, "Startup initiated...");
        PeerClient peerClient = new PeerClient();
        boolean prestart;
        String[] peerArray;
        
        serviceHosts = new HashMap();
        subscriptions = new HashMap();
        peerUrls = new ArrayList();
        try{
            prestart = false;
            
            // Check the given peer Urls if given 
            // Check the connection of the given peers and if the given peers are this host
            if (PEER_REGISTRAR_URL_LIST != null && !PEER_REGISTRAR_URL_LIST.isEmpty()) {
                if ((peerArray = PEER_REGISTRAR_URL_LIST.split(",")) != null && peerArray.length > 0) {
                    prestart = true;
                    listener = new RequestListener(PORT);
                    listener.startListener();

                    for (String peer : peerArray) {
                        currentPeer = peer;
                        peerCheck = false;
                        peerClient.connectivityCheck(peer, true);
                        if (!peerCheck) {
                            System.out.println("Adding Peer: " + peer);
                            peerUrls.add(peer);
                        }
                    }
                    currentPeer = null;
                }

                if (peers = !peerUrls.isEmpty()) {
                    peerSyncThread = new PeerSyncThread();
                    peerSyncThread.start();
                    while(!peerSyncThread.isSleeping()) {
                        if(!running) break;
                        Thread.sleep(1000);
                    }
                    monitorThread = new MonitorThread();
                    monitorThread.start();
                }
            } 
            
            // Read from serialized data if no peers found
            if (!peers) {
                readSerialized();
            }

            if(!prestart) {
                listener = new RequestListener(PORT);
                listener.startListener();
            }
            
            if (!peers) {
                monitorThread = new MonitorThread();
                monitorThread.start();
            }
        } catch(Exception e){
            LOGGER.log(Level.SEVERE, "Startup failed.");
            throw e;
        }
    }
    
    /**
     * Monitors the other threads
     */
    private static void monitoringThreads() {
        boolean pause = false;
        
        while(running){
            try {
                
                // Start PeerSyncThread if stopped
                if (peers && peerSyncThread.isStopped() && running) { 
                    LOGGER.log(Level.INFO, "Starting PeerSyncThread...");
                    System.out.println("Starting PeerSyncThread...");
                    peerSyncThread.start();
                }
                
                // Start ConfigMonitorThread if stopped
                if (monitorThread.isStopped() && running) { 
                    LOGGER.log(Level.INFO, "Starting MonitorThread...");
                    System.out.println("Starting MonitorThread...");
                    monitorThread.start();
                }
                
                // sleeps main thread for 60 seconds
                for(int i = 0; i < 60; i++){
                    if(!running) break;
                    Thread.sleep(1000);
                }
                
                // Interrupt PeerSyncThread if hung
                if (peers && peerSyncThread.isHung() && running) { 
                    LOGGER.log(Level.WARNING, "Interrupting PeerSyncThread...");
                    System.out.println("Interrupting PeerSyncThread...");
                    peerSyncThread.interrupt();
                    pause = true;
                }
                
                // Interrupt ConfigMonitorThread if hung
                if (monitorThread.isHung() && running) { 
                    LOGGER.log(Level.WARNING, "Interrupting MonitorThread...");
                    System.out.println("Interrupting MonitorThread...");
                    monitorThread.interrupt();
                    pause = true;
                }
                
                // pause main thread if other threads are interrupted
                if(pause) {
                    pause =  false;
                    for(int i = 0; i < 5; i++){
                        if(!running) break;
                        Thread.sleep(1000);
                    }
                }
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }
    
    /**
     * Shuts down the service cleanly
     */
    public static void shutdown(){
        System.out.println("Shut down initiated...");
        LOGGER.log(Level.INFO, "Shut down initiated...");
        
        LOGGER.log(Level.INFO, "Stopping request listener...");
        listener.stopListener();
        
        // Saving state
        saveSubscriptionsState();
        saveServiceHostState();
        
        LOGGER.log(Level.INFO, "Stopping monitor thread...");
        monitorThread.interrupt();
        
        LOGGER.log(Level.INFO, "Exiting.");
        System.exit(0);
    }
    
    /**
     * Save the State of the subscription field
     */
    public static void saveSubscriptionsState() {
        try{
            subscribeMutex.acquire();
            if(!subscriptionStateFile.exists()){
                subscriptionStateFile = new File("./subscription_state.ser");
                subscriptionStateFile.createNewFile();
            }
            try(ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(subscriptionStateFile))){
                if(subscriptionStateFile.canWrite()){
                    LOGGER.log(Level.INFO, "Saving current state of subscriptions...");
                    System.out.println("Saving current state of subscriptions...");
                    stream.writeObject(subscriptions);
                    LOGGER.log(Level.INFO, "Current state of subscriptions saved.");
                    System.out.println("Current state of subscriptions saved.");
                }
            }
        } catch(Exception e){
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        } finally {
            subscribeMutex.release();
        }
    }
    
    /**
     * Save the State of the serviceHosts field
     */
    public static void saveServiceHostState() {
        try{
            serviceMutex.acquire();
            if(!serviceHostStateFile.exists()){
                serviceHostStateFile = new File("./serviceHost_state.ser");
                serviceHostStateFile.createNewFile();
            }
            try(ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(serviceHostStateFile))){
                if(serviceHostStateFile.canWrite()){
                    LOGGER.log(Level.INFO, "Saving current state of serviceHost...");
                    System.out.println("Saving current state of serviceHost...");
                    stream.writeObject(serviceHosts);
                    LOGGER.log(Level.INFO, "Current state of serviceHost saved.");
                    System.out.println("Current state of serviceHost saved.");
                }
            }
        } catch(Exception e){
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        } finally {
            serviceMutex.release();
        }
    }
    
    /**
     * Read from Serialized files
     */
    public static void readSerialized() {
        System.out.println("Checking for preexisting serviceHost_state.ser...");
        LOGGER.log(Level.INFO, "Checking for preexisting serviceHost_state.ser...");
        if(serviceHostStateFile.exists()){
            if(serviceHostStateFile.canRead()){
                try(ObjectInputStream stream = new ObjectInputStream(new FileInputStream(serviceHostStateFile))){
                    serviceHosts = (HashMap)stream.readObject();
                    System.out.println("Success reading from preexisting serviceHost_state.ser.");
                    LOGGER.log(Level.INFO, "Success reading from preexisting serviceHost_state.ser.");
                } catch(Exception e){
                    LOGGER.log(Level.SEVERE, e.getMessage(), e);
                } 
            }
        }

        System.out.println("Checking for preexisting subscription_state.ser...");
        LOGGER.log(Level.INFO, "Checking for preexisting subscription_state.ser...");
        if(subscriptionStateFile.exists()){
            if(subscriptionStateFile.canRead()){
                try(ObjectInputStream stream = new ObjectInputStream(new FileInputStream(subscriptionStateFile))){
                    subscriptions = (HashMap)stream.readObject();
                    System.out.println("Success reading from preexisting subscription_state.ser.");
                    LOGGER.log(Level.INFO, "Success reading from preexisting subscription_state.ser.");
                } catch(Exception e){
                    LOGGER.log(Level.SEVERE, e.getMessage(), e);
                } 
            }
        }
    }
    
    public static boolean devOrPro (String ipAddress) {
        return ServiceConstants.DEVELOPING_TESTING ? ipAddressCheck(ipAddress) : networkCheck(ipAddress);
    }
    
    @Override
    public void sendEmail(String json){
    }
}
