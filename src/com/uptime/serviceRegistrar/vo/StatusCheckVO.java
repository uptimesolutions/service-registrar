/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.serviceRegistrar.vo;

import com.uptime.services.vo.ServiceHostVO;
import com.google.gson.JsonArray;
import java.io.Serializable;

/**
 *
 * @author twilcox
 */
public class StatusCheckVO implements Serializable{
    private boolean check;
    private JsonArray array;
    private SubscribeHostVO subscribeHost;
    private ServiceHostVO serviceHost;
    private String service;

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public JsonArray getArray() {
        return array;
    }

    public void setArray(JsonArray array) {
        this.array = array;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public SubscribeHostVO getSubscribeHost() {
        return subscribeHost;
    }

    public void setSubscribeHost(SubscribeHostVO subscribeHost) {
        this.subscribeHost = subscribeHost;
    }

    public ServiceHostVO getServiceHost() {
        return serviceHost;
    }

    public void setServiceHost(ServiceHostVO serviceHost) {
        this.serviceHost = serviceHost;
    }
}
