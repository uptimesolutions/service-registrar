/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.serviceRegistrar.vo;

import com.uptime.services.vo.ServiceHostVO;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author twilcox
 */
public class PeerVO {
    private String host;
    private Map<String,ArrayList<ServiceHostVO>> currentServiceHosts;
    private Map<String,ArrayList<SubscribeHostVO>> currentSubscriptions;
    private Map<String,ArrayList<ServiceHostVO>> missingServiceHosts;
    private Map<String,ArrayList<SubscribeHostVO>> missingSubscriptions;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Map<String, ArrayList<ServiceHostVO>> getCurrentServiceHosts() {
        return currentServiceHosts;
    }

    public void setCurrentServiceHosts(Map<String, ArrayList<ServiceHostVO>> currentServiceHosts) {
        this.currentServiceHosts = currentServiceHosts;
    }

    public Map<String, ArrayList<SubscribeHostVO>> getCurrentSubscriptions() {
        return currentSubscriptions;
    }

    public void setCurrentSubscriptions(Map<String, ArrayList<SubscribeHostVO>> currentSubscriptions) {
        this.currentSubscriptions = currentSubscriptions;
    }

    public Map<String, ArrayList<ServiceHostVO>> getMissingServiceHosts() {
        return missingServiceHosts;
    }

    public void setMissingServiceHosts(Map<String, ArrayList<ServiceHostVO>> missingServiceHosts) {
        this.missingServiceHosts = missingServiceHosts;
    }

    public Map<String, ArrayList<SubscribeHostVO>> getMissingSubscriptions() {
        return missingSubscriptions;
    }

    public void setMissingSubscriptions(Map<String, ArrayList<SubscribeHostVO>> missingSubscriptions) {
        this.missingSubscriptions = missingSubscriptions;
    }
}
