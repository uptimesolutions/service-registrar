/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.serviceRegistrar.http.listeners;

import com.sun.net.httpserver.HttpServer;
import static com.uptime.serviceRegistrar.ServiceRegistrar.LOGGER;
import com.uptime.serviceRegistrar.http.handlers.CircuitBreakerHandler;
import com.uptime.serviceRegistrar.http.handlers.PeerHandler;
import com.uptime.serviceRegistrar.http.handlers.QueryHandler;
import com.uptime.serviceRegistrar.http.handlers.RegistrationHandler;
import com.uptime.serviceRegistrar.http.handlers.SubscribeHandler;
import com.uptime.serviceRegistrar.http.handlers.SystemHandler;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.logging.Level;

/**
 *
 * @author ksimmons
 */
public class RequestListener {
    private int port = 0;
    HttpServer server;
    
    public RequestListener(int port){
        this.port = port;
    }
    
    public void stopListener(){
        LOGGER.log(Level.INFO, "Stopping request listener...");
        server.stop(10);
    }
    
    public void startListener() throws IOException{
        server = HttpServer.create(new InetSocketAddress(port), 2);
        server.createContext("/query", new QueryHandler());
        server.createContext("/register", new RegistrationHandler());
        server.createContext("/circuitbreaker", new CircuitBreakerHandler());
        server.createContext("/system", new SystemHandler());
        server.createContext("/subscribe", new SubscribeHandler());
        server.createContext("/peer", new PeerHandler());
        server.setExecutor(Executors.newCachedThreadPool());
        
        //start server
        LOGGER.log(Level.INFO, "Starting request listener...");
        server.start();

    }
}
