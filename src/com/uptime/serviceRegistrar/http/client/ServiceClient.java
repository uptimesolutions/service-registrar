/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.serviceRegistrar.http.client;


import static com.uptime.serviceRegistrar.ServiceRegistrar.LOGGER;
import static com.uptime.serviceRegistrar.ServiceRegistrar.subscribeMutex;
import static com.uptime.serviceRegistrar.ServiceRegistrar.subscriptions;
import com.uptime.serviceRegistrar.utils.QueryUtil;
import com.uptime.serviceRegistrar.vo.SubscribeHostVO;
import com.uptime.services.vo.ServiceHostVO;
import static com.uptime.services.util.ServiceUtil.ipAddressCheck;
import static com.uptime.services.util.ServiceUtil.portCheck;
import static com.uptime.services.util.ServiceUtil.sourceCheck;
import java.util.logging.Level;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;


/**
 *
 * @author ksimmons
 */
public class ServiceClient {
    private final int READ_TIMEOUT;
    private final int CONNECTION_TIMEOUT;
    private final QueryUtil queryUtil;
    
    /**
     * Constructor
     */
    public ServiceClient() {
        READ_TIMEOUT = 5000;
        CONNECTION_TIMEOUT = 5000;
        queryUtil = new QueryUtil();
    }
    
    /**
     * Update the serviceHosts field for subscribers of a given service name
     * @param service, String object
     */
    public void updateSubscribers(String service){
        if(subscriptions.containsKey(service)) {
            try{
                subscribeMutex.acquire();
                subscriptions.get(service).forEach(host -> updateSubscriber(service, host));    
            } catch(Exception e){
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
            } finally{
                subscribeMutex.release();
            }
        } 
    }
    
    /**
     * Update the serviceHosts field for the given subscriber of a given service name
     * @param name, the name of the service
     * @param subscribeHost, SubscribeHost objects
     */
    public void updateSubscriber (String name, SubscribeHostVO subscribeHost) {
        String json;
        RequestConfig config;
        HttpPost httpPost;
        HttpPut httpPut;
        int status = -1;
        
        try{
            if(sourceCheck(subscribeHost.getSource())) {
                // set the connection timeouts
                config = RequestConfig.custom()
                    .setConnectTimeout(CONNECTION_TIMEOUT)
                    .setConnectionRequestTimeout(READ_TIMEOUT)
                    .setSocketTimeout(READ_TIMEOUT).build();
                
                if (subscribeHost.getSource().equalsIgnoreCase("gui") && subscribeHost.getUrl() != null && !subscribeHost.getUrl().isEmpty()) {
                    // create the POST request
                    httpPost = new HttpPost(subscribeHost.getUrl());
                   
                    // create the JSON response
                    LOGGER.log(Level.INFO, "Sent POST: {0}", (json = queryUtil.queryJsonCreation(name)));
                    LOGGER.log(Level.INFO, "Sent To: {0}", subscribeHost.getUrl());
                    
                    // add the JSON to the request
                    httpPost.setEntity(new StringEntity(json));
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");
                    
                    // send the request
                    // get the status code
                    try(CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                            CloseableHttpResponse response = httpClient.execute(httpPost)){
                        status = response.getStatusLine().getStatusCode();
                    }

                    if (status != 200)
                        LOGGER.log(Level.WARNING, "Failed sending host list to GUI: HTTP response {0}", status);
                } else if (subscribeHost.getSource().equalsIgnoreCase("service") && ipAddressCheck(subscribeHost.getIpAddress()) && portCheck(subscribeHost.getPort())) {
                    // create the PUT request
                    httpPut = new HttpPut("http://" + subscribeHost.getIpAddress() + ":" + String.valueOf(subscribeHost.getPort()) + "/system");
                    
                    // create the JSON
                    LOGGER.log(Level.INFO, "Sent PUT: {0}", (json = queryUtil.queryJsonCreation(name)));
                    LOGGER.log(Level.INFO, "Sent To: http://{0}:{1}/system", new Object[]{subscribeHost.getIpAddress(), String.valueOf(subscribeHost.getPort())});
                    
                    // add the JSON to the request
                    httpPut.setEntity(new StringEntity(json));
                    httpPut.setHeader("Accept", "application/json");
                    httpPut.setHeader("Content-type", "application/json");
                    
                    // send the request
                    // get the status code
                    try(CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                            CloseableHttpResponse response = httpClient.execute(httpPut)){
                        status = response.getStatusLine().getStatusCode();
                    }
                    
                    if (status != 200)
                        LOGGER.log(Level.WARNING, "Failed sending host list to Service: HTTP response {0}", status);
                }
            }
        }catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     * Checks the given host's connectivity
     * @param host, ServiceHostVO object
     * @return boolean, true if proper json received, else false
     */
    public boolean connectivityCheck(ServiceHostVO host){
        HttpGet httpGet;
        
        try {
            if(ipAddressCheck(host.getIp()) && portCheck(host.getPort())) {
                // set the connection timeouts
                RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(CONNECTION_TIMEOUT)
                    .setConnectionRequestTimeout(READ_TIMEOUT)
                    .setSocketTimeout(READ_TIMEOUT).build();
                
                // create the GET request
                httpGet = new HttpGet("http://"+host.getIp()+":"+String.valueOf(host.getPort())+"/system");
                httpGet.setHeader("Accept", "application/json");
                
                // send the request
                // get the status code
                try(CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                        CloseableHttpResponse response = httpClient.execute(httpGet)){
                    return response.getStatusLine().getStatusCode() == 200;
                }
            }
        }
        catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return false;
    }
}
