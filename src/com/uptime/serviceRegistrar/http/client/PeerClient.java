/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.serviceRegistrar.http.client;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.uptime.serviceRegistrar.vo.StatusCheckVO;
import com.uptime.serviceRegistrar.vo.SubscribeHostVO;
import com.uptime.services.vo.ServiceHostVO;
import static com.uptime.serviceRegistrar.ServiceRegistrar.LOGGER;
import static com.uptime.serviceRegistrar.ServiceRegistrar.peerUrls;
import static com.uptime.services.util.ServiceUtil.ipAddressCheck;
import static com.uptime.services.util.ServiceUtil.portCheck;
import static com.uptime.services.util.ServiceUtil.sourceCheck;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author twilcox
 */
public class PeerClient {
    private final int READ_TIMEOUT;
    private final int CONNECTION_TIMEOUT;

    /**
     * Constructor
     */
    public PeerClient() {
        READ_TIMEOUT = 5000;
        CONNECTION_TIMEOUT = 5000;
    }
    
    /**
     * POST http command
     * Send a subscribe to a Peer
     * @param content, String object
     */
    public void subscribe(String content) {
        RequestConfig config;
        HttpPost httpPost;
        int status = -1;
        
        if (peerUrls != null && !peerUrls.isEmpty()) {
            for (String peer : peerUrls) {
                try {
                    if (peer != null && !peer.isEmpty()) {
                        // set the connection timeouts
                        config = RequestConfig.custom()
                            .setConnectTimeout(CONNECTION_TIMEOUT)
                            .setConnectionRequestTimeout(READ_TIMEOUT)
                            .setSocketTimeout(READ_TIMEOUT).build();

                        // create the POST request
                        httpPost = new HttpPost(peer + "/peer?action=subscribe");

                        // add the JSON to the request
                        httpPost.setEntity(new StringEntity(content));
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        LOGGER.log(Level.INFO, "Sent POST: {0}", content);
                        LOGGER.log(Level.INFO, "Sent To: {0}", peer + "/peer?action=subscribe");

                        // send the request
                        // get the status code
                        try(CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                                CloseableHttpResponse response = httpClient.execute(httpPost)){
                            status = response.getStatusLine().getStatusCode();
                        }
                        
                        if (status != 200)
                            LOGGER.log(Level.WARNING, "Failed sending subscribe to Peer: HTTP response {0}", status);
                    }
                }catch (Exception e) {
                    LOGGER.log(Level.SEVERE, e.getMessage(), e);
                    System.out.println("Exception in peer subscribe:" + e.getMessage());
                }
            }
        }
    }
    
    /**
     * POST http command
     * Send a subscribe to a Peer
     * @param peer, String object
     * @param subscriptionMap, Map of Key = String objects and Value = ArrayList of SubscribeHostVO objects
     */
    public void subscribe(String peer, Map<String,ArrayList<SubscribeHostVO>> subscriptionMap) {
        RequestConfig config;
        HttpPost httpPost;
        int status = -1;
        StringBuilder json;
        
        if (peer != null && !peer.isEmpty()) {
            for (String key : subscriptionMap.keySet()) {
                for (SubscribeHostVO subscription : subscriptionMap.get(key)) {
                    try {
                        json = new StringBuilder();
                        json
                            .append("{\"names\":[{\"name\":\"").append(key).append("\"}],")  
                            .append("\"ipAddress\":\"").append(subscription.getIpAddress()).append("\",")
                            .append("\"url\":\"").append(subscription.getUrl()).append("\",")
                            .append("\"port\":\"").append(subscription.getPort()).append("\",")
                            .append("\"source\":\"").append(subscription.getSource()).append("\"}");
                        
                        // set the connection timeouts
                        config = RequestConfig.custom()
                            .setConnectTimeout(CONNECTION_TIMEOUT)
                            .setConnectionRequestTimeout(READ_TIMEOUT)
                            .setSocketTimeout(READ_TIMEOUT).build();

                        // create the POST request
                        httpPost = new HttpPost(peer + "/peer?action=subscribe");

                        // add the JSON to the request
                        httpPost.setEntity(new StringEntity(json.toString()));
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        LOGGER.log(Level.INFO, "Sent POST: {0}", json.toString());
                        LOGGER.log(Level.INFO, "Sent To: {0}", peer + "/peer?action=subscribe");

                        // send the request
                        // get the status code
                        try(CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                                CloseableHttpResponse response = httpClient.execute(httpPost)){
                            status = response.getStatusLine().getStatusCode();
                        }
                        
                        if (status != 200)
                            LOGGER.log(Level.WARNING, "Failed sending subscribe to Peer: HTTP response {0}", status);
                    }catch (Exception e) {
                        LOGGER.log(Level.WARNING, e.getMessage(), e);
                        System.out.println("Exception in peer subscribe:" + e.getMessage());
                    }
                }
            }
        }
    }
    
    /**
     * PUT http command
     * Send an unsubscribe to a Peer
     * @param content, String object
     */
    public void unsubscribe(String content) {
        RequestConfig config;
        HttpPut httpPut;
        int status = -1;
        
        if (peerUrls != null && !peerUrls.isEmpty()) {
            for (String peer : peerUrls) {
                try {
                    if (peer != null && !peer.isEmpty()) {
                        // set the connection timeouts
                        config = RequestConfig.custom()
                            .setConnectTimeout(CONNECTION_TIMEOUT)
                            .setConnectionRequestTimeout(READ_TIMEOUT)
                            .setSocketTimeout(READ_TIMEOUT).build();

                        // create the Put request
                        httpPut = new HttpPut(peer + "/peer?action=unsubscribe");

                        // add the JSON to the request
                        httpPut.setEntity(new StringEntity(content));
                        httpPut.setHeader("Accept", "application/json");
                        httpPut.setHeader("Content-type", "application/json");

                        LOGGER.log(Level.INFO, "Sent PUT: {0}", content);
                        LOGGER.log(Level.INFO, "Sent To: {0}", peer + "/peer?action=unsubscribe");

                        // send the request
                        // get the status code
                        try(CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                                CloseableHttpResponse response = httpClient.execute(httpPut)){
                            status = response.getStatusLine().getStatusCode();
                        }
                        
                        if (status != 200)
                            LOGGER.log(Level.WARNING, "Failed sending unsubscibe to Peer: HTTP response {0}", status);
                    }
                }catch (Exception e) {
                    LOGGER.log(Level.SEVERE, e.getMessage(), e);
                    System.out.println("Exception in peer unsubscribe:" + e.getMessage());
                }
            }
        }
    }
    
    /**
     * POST http command
     * Sends a register to a Peer
     * @param content, String object
     */
    public void register(String content) {
        RequestConfig config;
        HttpPost httpPost;
        int status = -1;
        
        if (peerUrls != null && !peerUrls.isEmpty()) {
            for (String peer : peerUrls) {
                try {
                    if (peer != null && !peer.isEmpty()) {
                        // set the connection timeouts
                        config = RequestConfig.custom()
                            .setConnectTimeout(CONNECTION_TIMEOUT)
                            .setConnectionRequestTimeout(READ_TIMEOUT)
                            .setSocketTimeout(READ_TIMEOUT).build();

                        // create the POST request
                        httpPost = new HttpPost(peer + "/peer?action=register");

                        // add the JSON to the request
                        httpPost.setEntity(new StringEntity(content));
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        LOGGER.log(Level.INFO, "Sent POST: {0}", content);
                        LOGGER.log(Level.INFO, "Sent To: {0}", peer + "/peer?action=register");

                        // send the request
                        // get the status code
                        try(CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                                CloseableHttpResponse response = httpClient.execute(httpPost)){
                            status = response.getStatusLine().getStatusCode();
                        }
                        
                        if (status != 200)
                            LOGGER.log(Level.WARNING, "Failed sending register to Peer: HTTP response {0}", status);
                    }
                }catch (Exception e) {
                    LOGGER.log(Level.SEVERE, e.getMessage(), e);
                    System.out.println("Exception in peer register:" + e.getMessage());
                }
            }
        }
    }
    
    /**
     * POST http command
     * Sends a register to a Peer
     * @param peer, String object
     * @param serviceHostMap, Map of Key = String objects and Value = ArrayList of ServiceHostMap objects
     */
    public void register(String peer, Map<String,ArrayList<ServiceHostVO>> serviceHostMap) {
        RequestConfig config;
        HttpPost httpPost;
        int status = -1;
        StringBuilder json;
        
        if (peer != null && !peer.isEmpty()) {
            for (String key : serviceHostMap.keySet()) {
                for (ServiceHostVO serviceHost : serviceHostMap.get(key)) {
                    try {
                        json = new StringBuilder();
                        json
                            .append("{\"name\":\"").append(key).append("\",")
                            .append("\"circuitBreaker\":\"").append(serviceHost.isCircuitBreaker()).append("\",")
                            .append("\"ipAddress\":\"").append(serviceHost.getIp()).append("\",")
                            .append("\"port\":\"").append(serviceHost.getPort()).append("\"}");
                        
                        // set the connection timeouts
                        config = RequestConfig.custom()
                            .setConnectTimeout(CONNECTION_TIMEOUT)
                            .setConnectionRequestTimeout(READ_TIMEOUT)
                            .setSocketTimeout(READ_TIMEOUT).build();

                        // create the POST request
                        httpPost = new HttpPost(peer + "/peer?action=register");

                        // add the JSON to the request
                        httpPost.setEntity(new StringEntity(json.toString()));
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        LOGGER.log(Level.INFO, "Sent POST: {0}", json.toString());
                        LOGGER.log(Level.INFO, "Sent To: {0}", peer + "/peer?action=register");

                        // send the request
                        // get the status code
                        try(CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                                CloseableHttpResponse response = httpClient.execute(httpPost)){
                            status = response.getStatusLine().getStatusCode();
                        }
                        
                        if (status != 200)
                            LOGGER.log(Level.WARNING, "Failed sending register to Peer: HTTP response {0}", status);
                    }catch (Exception e) {
                        LOGGER.log(Level.WARNING, e.getMessage(), e);
                        System.out.println("Exception in peer register:" + e.getMessage());
                    }
                }
            }
        }
    }
    
    /**
     * DELETE http command
     * Sends an unregister to a Peer
     * @param statusCheckVO, StatusCheckVO object
     */
    public void unregister(StatusCheckVO statusCheckVO) {
        RequestConfig config;
        HttpDelete httpDelete;
        StringBuilder url;
        int status = -1;
        
        if (peerUrls != null && !peerUrls.isEmpty()) {
            for (String peer : peerUrls) {
                try {
                    if (peer != null && !peer.isEmpty()) {
                        url = new StringBuilder();
                        url
                            .append(peer)
                            .append("/peer?action=unregister")
                            .append("&name=").append(statusCheckVO.getService())
                            .append("&port=").append(statusCheckVO.getServiceHost().getPort())
                            .append("&ipAddress=").append(statusCheckVO.getServiceHost().getIp());
                        
                        // set the connection timeouts
                        config = RequestConfig.custom()
                            .setConnectTimeout(CONNECTION_TIMEOUT)
                            .setConnectionRequestTimeout(READ_TIMEOUT)
                            .setSocketTimeout(READ_TIMEOUT).build();

                        LOGGER.log(Level.INFO, "Sent DELETE To: {0}", url.toString());
                        
                        // create the DELETE request
                        httpDelete = new HttpDelete(url.toString());
                        httpDelete.setHeader("Accept", "application/json");

                        // send the request
                        // get the status code
                        try(CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                                CloseableHttpResponse response = httpClient.execute(httpDelete)){
                            status = response.getStatusLine().getStatusCode();
                        }
                        
                        if (status != 200)
                            LOGGER.log(Level.WARNING, "Failed sending unregister to Peer: HTTP response {0}", status);
                    }
                }catch (Exception e) {
                    LOGGER.log(Level.SEVERE, e.getMessage(), e);
                    System.out.println("Exception in peer unregister:" + e.getMessage());
                }
            }
        }
    }
    
    /**
     * POST http command
     * Sends a circuitBreaker to a Peer
     * @param content, String object
     */
    public void addCircuitBreaker(String content) {
        RequestConfig config;
        HttpPost httpPost;
        int status = -1;
        
        if (peerUrls != null && !peerUrls.isEmpty()) {
            for (String peer : peerUrls) {
                try {
                    if (peer != null && !peer.isEmpty()) {
                        // set the connection timeouts
                        config = RequestConfig.custom()
                            .setConnectTimeout(CONNECTION_TIMEOUT)
                            .setConnectionRequestTimeout(READ_TIMEOUT)
                            .setSocketTimeout(READ_TIMEOUT).build();

                        // create the POST request
                        httpPost = new HttpPost(peer + "/peer?action=circuitBreaker");

                        // add the JSON to the request
                        httpPost.setEntity(new StringEntity(content));
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        LOGGER.log(Level.INFO, "Sent POST: {0}", content);
                        LOGGER.log(Level.INFO, "Sent To: {0}", peer + "/peer?action=circuitBreaker");

                        // send the request
                        // get the status code
                        try(CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                                CloseableHttpResponse response = httpClient.execute(httpPost)){
                            status = response.getStatusLine().getStatusCode();
                        }

                        if (status != 200)
                            LOGGER.log(Level.WARNING, "Failed sending circuitBreaker to Peer: HTTP response {0}", status);
                    }
                }catch (Exception e) {
                    LOGGER.log(Level.SEVERE, e.getMessage(), e);
                    System.out.println("Exception in peer addCircuitBreaker:" + e.getMessage());
                }
            }
        }
    }
    
    /**
     * DELETE http command
     * Sends an circuitBreaker to a Peer
     * @param statusCheckVO, StatusCheckVO object
     */
    public void removeCircuitBreaker(StatusCheckVO statusCheckVO) {
        RequestConfig config;
        HttpDelete httpDelete;
        StringBuilder url;
        int status = -1;
        
        if (peerUrls != null && !peerUrls.isEmpty()) {
            for (String peer : peerUrls) {
                try {
                    if (peer != null && !peer.isEmpty()) {
                        url = new StringBuilder();
                        url
                            .append(peer)
                            .append("/peer?action=circuitBreaker")
                            .append("&name=").append(statusCheckVO.getService())
                            .append("&port=").append(statusCheckVO.getServiceHost().getPort())
                            .append("&ipAddress=").append(statusCheckVO.getServiceHost().getIp());
                        
                        // set the connection timeouts
                        config = RequestConfig.custom()
                            .setConnectTimeout(CONNECTION_TIMEOUT)
                            .setConnectionRequestTimeout(READ_TIMEOUT)
                            .setSocketTimeout(READ_TIMEOUT).build();

                        LOGGER.log(Level.INFO, "Sent DELETE To: {0}", url.toString());
                        
                        // create the DELETE request
                        httpDelete = new HttpDelete(url.toString());
                        httpDelete.setHeader("Accept", "application/json");

                        // send the request
                        // get the status code
                        try(CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                                CloseableHttpResponse response = httpClient.execute(httpDelete)){
                            status = response.getStatusLine().getStatusCode();
                        }

                        if (status != 200)
                            LOGGER.log(Level.WARNING, "Failed sending remove circuitBreaker to Peer: HTTP response {0}", status);
                    }
                }catch (Exception e) {
                    LOGGER.log(Level.SEVERE, e.getMessage(), e);
                    System.out.println("Exception in peer removeCircuitBreaker:" + e.getMessage());
                }
            }
        }
    }
    
    /**
     * GET http command
 Get the ServiceHostVO list from the given pear
     * @param peer, String object
     * @return Map, Key = String object, Value = ArrayList of ServiceHostVO objects
     * @throws Exception
     */
    public Map<String,ArrayList<ServiceHostVO>> getServiceHosts (String peer) throws Exception {
        Map<String,ArrayList<ServiceHostVO>> map = new HashMap();
        RequestConfig config;
        HttpGet httpGet;
        HttpEntity entity;
        String content, name;
        JsonElement element;
        JsonObject json;
        JsonArray services, hosts;
        ArrayList<ServiceHostVO> serviceHostList;
        ServiceHostVO serviceHost;
        int status = -1;
        
        try {
            if (peer != null && !peer.isEmpty()) {
                // set the connection timeouts
                config = RequestConfig.custom()
                    .setConnectTimeout(CONNECTION_TIMEOUT)
                    .setConnectionRequestTimeout(READ_TIMEOUT)
                    .setSocketTimeout(READ_TIMEOUT).build();

                // create the GET request
                httpGet = new HttpGet(peer + "/system?serviceCommand=serviceHosts");
                httpGet.setHeader("Accept", "application/json");

                LOGGER.log(Level.INFO, "Sent GET To: {0}", peer + "/system?serviceCommand=serviceHosts");

                // send the request
                // get the status code and return json
                content = null;
                try(CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                        CloseableHttpResponse response = httpClient.execute(httpGet)){
                    status = response.getStatusLine().getStatusCode();
                    if ((entity = response.getEntity()) != null) {
                        content = EntityUtils.toString(entity);
                    }
                }
                
                if (status != 200) {
                    LOGGER.log(Level.WARNING, "Failed getting ServiceHost list from Peer: HTTP response {0}", status);
                    System.out.println("Failed getting ServiceHost list from Peer: HTTP response " + String.valueOf(status));
                    map = null;
                } else {
                    if(content != null && !content.isEmpty()) {
                        if ((element = JsonParser.parseString(content)) != null && (json = element.getAsJsonObject()) != null && json.has("serviceHosts")){
                            if ((services = json.getAsJsonArray("serviceHosts")) != null) {
                                for (int i = 0; i < services.size(); i++) {
                                    if ((element = services.get(i)) != null && (json = element.getAsJsonObject()) != null && json.has("service") && json.has("hosts")){
                                        if ((name = json.get("service").getAsString()) != null && (hosts = json.getAsJsonArray("hosts")) != null) {
                                            serviceHostList = new ArrayList();
                                            for (int j = 0; j < hosts.size(); j++) {
                                                try {
                                                    if ((element = hosts.get(j)) != null && (json = element.getAsJsonObject()) != null && json.has("ipAddress") && json.has("port") && json.has("circuitBreaker") && ipAddressCheck(json.get("ipAddress").getAsString()) && portCheck(json.get("port").getAsInt())){
                                                        serviceHost = new ServiceHostVO();
                                                        serviceHost.setIp(json.get("ipAddress").getAsString());
                                                        serviceHost.setPort(json.get("port").getAsInt());
                                                        serviceHost.setCircuitBreaker(json.get("circuitBreaker").getAsBoolean());
                                                        serviceHostList.add(serviceHost);
                                                    }
                                                }catch (Exception e){}
                                            }
                                            if (!serviceHostList.isEmpty() && !map.containsKey(name))
                                                map.put(name, serviceHostList);
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        LOGGER.log(Level.WARNING, "Failed getting ServiceHost entity from Peer");
                        System.out.println("Failed getting ServiceHost entity from Peer");
                    }
                }
            }
        }catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            System.out.println("Exception in peer getServiceHosts:" + e.getMessage());
            throw e;
        }
        return map;
    }
    
    /**
     * GET http command
     * Get the Subscription list from the given pear
     * @param peer, String object
     * @return Map, Key = String object, Value = ArrayList of SubscribeHostVO objects
     * @throws Exception
     */
    public Map<String,ArrayList<SubscribeHostVO>> getSubscriptions (String peer) throws Exception  {
        Map<String,ArrayList<SubscribeHostVO>> map = new HashMap();
        RequestConfig config;
        HttpGet httpGet;
        HttpEntity entity;
        JsonElement element;
        JsonObject json;
        JsonArray subscriptionArray, subscribeHostArray;
        ArrayList<SubscribeHostVO> subscribeHostVOList;
        SubscribeHostVO subscriber;
        String ipAddress, source, url, content, name;
        int status = -1, port;
        
        try {
            if (peer != null && !peer.isEmpty()) {
                // set the connection timeouts
                config = RequestConfig.custom()
                    .setConnectTimeout(CONNECTION_TIMEOUT)
                    .setConnectionRequestTimeout(READ_TIMEOUT)
                    .setSocketTimeout(READ_TIMEOUT).build();

                // create the GET request
                httpGet = new HttpGet(peer + "/system?serviceCommand=subscriptions");
                httpGet.setHeader("Accept", "application/json");

                LOGGER.log(Level.INFO, "Sent GET To: {0}", peer + "/system?serviceCommand=subscriptions");

                // send the request
                // get the status code and return json
                content = null;
                try(CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                        CloseableHttpResponse response = httpClient.execute(httpGet)){
                    status = response.getStatusLine().getStatusCode();
                    if ((entity = response.getEntity()) != null) {
                        content = EntityUtils.toString(entity);
                    }
                }
                
                if (status != 200) {
                    LOGGER.log(Level.WARNING, "Failed getting Subscription list from Peer: HTTP response {0}", status);
                    System.out.println("Failed getting Subscription list from Peer: HTTP response " + String.valueOf(status));
                    map = null;
                } else {
                    if(content != null && !content.isEmpty()) {
                        if ((element = JsonParser.parseString(content)) != null && (json = element.getAsJsonObject()) != null && json.has("subscriptions")) {
                            if ((subscriptionArray = json.getAsJsonArray("subscriptions")) != null) {
                                for (int i = 0; i < subscriptionArray.size(); i++) {
                                    if ((element = subscriptionArray.get(i)) != null && (json = element.getAsJsonObject()) != null && json.has("service") && json.has("hosts")){
                                        if ((name = json.get("service").getAsString()) != null && (subscribeHostArray = json.getAsJsonArray("hosts")) != null) {
                                            subscribeHostVOList = new ArrayList();
                                            for (int j = 0; j < subscribeHostArray.size(); j++) {
                                                if ((element = subscribeHostArray.get(j)) != null && (json = element.getAsJsonObject()) != null && json.has("ipAddress") && json.has("port") && json.has("url") && json.has("source")){
                                                    try {
                                                        subscriber = new SubscribeHostVO();
                                                        subscriber.setIpAddress(ipAddressCheck(ipAddress = json.get("ipAddress").getAsString()) ? ipAddress : "");
                                                        subscriber.setPort(portCheck(port = json.get("port").getAsInt()) ? port : 0);
                                                        subscriber.setUrl(((url = json.get("url").getAsString()) != null && !url.isEmpty()) ? url : "");
                                                        subscriber.setSource(sourceCheck(source = json.get("source").getAsString()) ? source : "");
                                                        if (subscriber.getSource().equalsIgnoreCase("gui") && subscriber.getUrl() != null && !subscriber.getUrl().isEmpty()) {
                                                            subscribeHostVOList.add(subscriber);
                                                        }else if (subscriber.getSource().equalsIgnoreCase("service") && ipAddressCheck(subscriber.getIpAddress()) && portCheck(subscriber.getPort())) {
                                                            subscribeHostVOList.add(subscriber);
                                                        }
                                                    }catch (Exception e){}
                                                }
                                            }
                                            if (!subscribeHostVOList.isEmpty() && !map.containsKey(name))
                                                map.put(name, subscribeHostVOList);
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        LOGGER.log(Level.WARNING, "Failed getting Subscription entity from Peer");
                        System.out.println("Failed getting Subscription entity from Peer");
                    }
                }
            }
        }catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            System.out.println("Exception in peer getSubscriptions:" + e.getMessage());
            throw e;
        }
        return map;
    }
    
    /**
     * Checks the given peer's connectivity
     * @param peer, String object
     * @param postCheck, boolean, true if POST, else GET
     * @return boolean
     */
    public boolean connectivityCheck(String peer, boolean postCheck){
        RequestConfig config;
        StringBuilder json;
        HttpGet httpGet;
        HttpPost httpPost;
        int status = -1;
        
        if (peer != null && !peer.isEmpty()) {
            // set the connection timeouts
            config = RequestConfig.custom()
                .setConnectTimeout(CONNECTION_TIMEOUT)
                .setConnectionRequestTimeout(READ_TIMEOUT)
                .setSocketTimeout(READ_TIMEOUT).build();
            
            if (postCheck) {
                try {
                    json = new StringBuilder();
                    json.append("{\"peer\":\"").append(peer).append("\"}");
                    
                    // create the POST request
                    httpPost = new HttpPost(peer + "/peer?action=connectionCheck");
                    
                    // add the JSON to the request
                    httpPost.setEntity(new StringEntity(json.toString()));
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");
                    
                    LOGGER.log(Level.INFO, "Sent POST: {0}", json.toString());
                    LOGGER.log(Level.INFO, "Sent To: {0}", peer + "/peer?action=connectionCheck");

                    // send the request
                    // get the status code
                    try(CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                            CloseableHttpResponse response = httpClient.execute(httpPost)){
                        status = response.getStatusLine().getStatusCode();
                    }

                    return status != 200;
                }catch (Exception ex) {}
            } else {
                try {
                
                    // create the GET request
                    httpGet = new HttpGet(peer + "/system");
                    httpGet.setHeader("Accept", "application/json");

                    // send the request
                    // get the status code
                    try(CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
                            CloseableHttpResponse response = httpClient.execute(httpGet)){
                        status = response.getStatusLine().getStatusCode();
                    }

                    return status == 200;
                } catch (Exception ex) {}
            }
        }
        return false;
    }
}
