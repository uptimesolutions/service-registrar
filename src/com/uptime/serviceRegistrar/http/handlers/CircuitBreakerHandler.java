/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.serviceRegistrar.http.handlers;

import com.sun.net.httpserver.HttpExchange;
import static com.uptime.serviceRegistrar.ServiceRegistrar.LOGGER;
import static com.uptime.serviceRegistrar.ServiceRegistrar.running;
import com.uptime.serviceRegistrar.http.client.PeerClient;
import com.uptime.serviceRegistrar.http.client.ServiceClient;
import com.uptime.serviceRegistrar.utils.CircuitBreakerUtil;
import com.uptime.serviceRegistrar.vo.StatusCheckVO;
import com.uptime.services.http.handler.AbstractGenericHandler;
import static com.uptime.services.util.HttpUtils.parseQuery;
import static com.uptime.services.util.ServiceUtil.streamReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ksimmons
 */
public class CircuitBreakerHandler extends AbstractGenericHandler {
    private final PeerClient peerClient;
    private final ServiceClient serviceClient;
    private final CircuitBreakerUtil circuitBreakerUtil;
    
    /**
     * Constructor
     */
    public CircuitBreakerHandler() {
        peerClient = new PeerClient();
        serviceClient = new ServiceClient();
        circuitBreakerUtil = new CircuitBreakerUtil();
    }
    
    /**
     * HTTP POST handler
     * @param he
     * @throws IOException 
     */
    @Override
    public void doPost(HttpExchange he) throws IOException {
        StatusCheckVO statusCheckVO = new StatusCheckVO();
        String content = null;

        try {
            if((content = streamReader(he.getRequestBody())) != null && !content.isEmpty()) {
                LOGGER.log(Level.INFO, "CircuitBreaker received POST Json: {0}", content);
                he.sendResponseHeaders(circuitBreakerUtil.setStatuesCheckFromJson(content, statusCheckVO) ? HttpURLConnection.HTTP_OK : HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
            } else 
                he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
        } catch (Exception e) {
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        he.close();
        
        if(statusCheckVO.getService() != null && statusCheckVO.getServiceHost() != null) {
            circuitBreakerUtil.setCircuitBreaker(statusCheckVO.getService(),statusCheckVO.getServiceHost(),true);
            serviceClient.updateSubscribers(statusCheckVO.getService());
            if (content != null)
                peerClient.addCircuitBreaker(content);
        }
    }
    
    /**
     * HTTP DELETE handler
     * @param he
     * @throws IOException 
     */
    @Override
    public void doDelete(HttpExchange he) throws IOException {
        StatusCheckVO statusCheckVO = new StatusCheckVO();
        Map<String, Object> params;
                
        try{
            LOGGER.log(Level.INFO, "CircuitBreaker received DELETE request: {0}", he.getRequestURI().getRawQuery());
            if((params = parseQuery(he.getRequestURI().getRawQuery())) != null && !params.isEmpty())
                he.sendResponseHeaders(circuitBreakerUtil.setStatuesCheckFromParams(params, statusCheckVO) ? HttpURLConnection.HTTP_OK : HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
            else
                he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
        }catch (Exception e) {
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        he.close();
        
        if(statusCheckVO.getService() != null && statusCheckVO.getServiceHost() != null) {
            circuitBreakerUtil.setCircuitBreaker(statusCheckVO.getService(),statusCheckVO.getServiceHost(),false); 
            peerClient.removeCircuitBreaker(statusCheckVO); 
            serviceClient.updateSubscribers(statusCheckVO.getService());
        }
    }

    @Override
    public boolean getRunning() {
        return running;
    }

    @Override
    public Logger getLogger() {
        return LOGGER;
    }
}
