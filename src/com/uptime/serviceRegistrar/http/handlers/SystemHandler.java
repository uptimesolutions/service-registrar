/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.serviceRegistrar.http.handlers;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import static com.uptime.serviceRegistrar.ServiceRegistrar.IP_ADDRESS;
import static com.uptime.serviceRegistrar.ServiceRegistrar.LOGGER;
import static com.uptime.serviceRegistrar.ServiceRegistrar.PORT;
import static com.uptime.serviceRegistrar.ServiceRegistrar.SERVICE_NAME;
import static com.uptime.serviceRegistrar.ServiceRegistrar.running;
import com.uptime.serviceRegistrar.utils.SystemUtil;
import static com.uptime.services.AbstractService.getServiceInfo;
import static com.uptime.services.AbstractService.getStatistics;
import com.uptime.services.http.handler.AbstractGenericHandler;
import static com.uptime.services.util.HttpUtils.parseQuery;
import static com.uptime.services.util.ServiceUtil.streamReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ksimmons
 */
public class SystemHandler extends AbstractGenericHandler {
    private final SystemUtil systemUtil;

    /**
     * Constructor
     */
    public SystemHandler() {
        systemUtil = new SystemUtil();
    }
    
    /**
     * HTTP GET handler
     * @param he
     * @throws IOException 
     */
    @Override
    public void doGet(HttpExchange he) throws IOException {
        Map<String, Object> params;
        byte [] response;
        OutputStream os;
        Headers respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        String resp;
        
        try{
            LOGGER.log(Level.INFO, "System received GET request: {0}", he.getRequestURI().getRawQuery());
            System.out.println("System received GET request: " + he.getRequestURI().getRawQuery());
            params = parseQuery(he.getRequestURI().getRawQuery());
            if(params.isEmpty()){
                resp = getServiceInfo(SERVICE_NAME, IP_ADDRESS, PORT);
            } else if(params.get("serviceCommand") != null) {
                switch (params.get("serviceCommand").toString().toLowerCase()) {
                    case"activity":
                        resp = getStatistics();
                        break;
                    case"servicehosts":
                        resp = systemUtil.serviceHostsToJson();
                        break;
                    case"subscriptions":
                        resp = systemUtil.subscriptionsToJson();
                        break;
                    default:
                        resp = "{\"outcome\":\"Unknown Value\"}";
                }
            }else{
                resp = "{\"outcome\":\"Unknown Params\"}";
            }
            
            if(resp.contains("Unknown Params") || resp.contains("Unknown Value")) { 
                response = resp.getBytes();
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, response.length);
            } else {
                response = resp.getBytes();
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            }
        } catch (UnsupportedEncodingException e) {
            response = "{\"outcome\":\"UnsupportedEncodingException\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, response.length);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        } catch (Exception e) {
            response = "{\"outcome\":\"Exception\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, response.length);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();
    }
    
    /**
     * HTTP POST handler
     * @param he
     * @throws IOException 
     */
    @Override
    public void doPost(HttpExchange he) throws IOException {
        String content;

        try {
            if((content = streamReader(he.getRequestBody())) != null && !content.isEmpty()) {
                LOGGER.log(Level.INFO, "System received POST Json: {0}", content);
                he.sendResponseHeaders(systemUtil.postCommand(content) ? HttpURLConnection.HTTP_OK : HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
            } else 
                he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
        } catch (Exception e) {
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        he.close();
    }
    
    /**
     * HTTP PUT handler
     * @param he
     * @throws IOException 
     */
    @Override
    public void doPut(HttpExchange he) throws IOException {
        String content;

        try {
            if((content = streamReader(he.getRequestBody())) != null && !content.isEmpty()) {
                LOGGER.log(Level.INFO, "System received PUT Json: {0}", content);
                he.sendResponseHeaders(systemUtil.putCommand(content) ? HttpURLConnection.HTTP_OK : HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
            } else 
                he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
        } catch (Exception e) {
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        he.close();
    }

    @Override
    public boolean getRunning() {
        return running;
    }

    @Override
    public Logger getLogger() {
        return LOGGER;
    }
}
