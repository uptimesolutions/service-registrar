/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.serviceRegistrar.http.handlers;

import com.sun.net.httpserver.HttpExchange;
import static com.uptime.serviceRegistrar.ServiceRegistrar.LOGGER;
import static com.uptime.serviceRegistrar.ServiceRegistrar.running;
import static com.uptime.serviceRegistrar.ServiceRegistrar.saveSubscriptionsState;
import static com.uptime.serviceRegistrar.ServiceRegistrar.subscribeMutex;
import com.uptime.serviceRegistrar.http.client.PeerClient;
import com.uptime.serviceRegistrar.http.client.ServiceClient;
import com.uptime.serviceRegistrar.utils.SubscribeUtil;
import com.uptime.serviceRegistrar.vo.StatusCheckVO;
import com.uptime.services.http.handler.AbstractGenericHandler;
import static com.uptime.services.util.ServiceUtil.streamReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ksimmons
 */
public class SubscribeHandler extends AbstractGenericHandler {
    private final PeerClient peerClient;
    private final ServiceClient serviceClient;
    private final SubscribeUtil subscribeUtil;
    
    /**
     * Constructor
     */
    public SubscribeHandler() {
        peerClient = new PeerClient();
        serviceClient = new ServiceClient();
        subscribeUtil = new SubscribeUtil();
    }
    
    /**
     * HTTP POST handler
     * @param he
     * @throws IOException 
     */
    @Override
    public void doPost(HttpExchange he) throws IOException {
        StatusCheckVO statusCheckVO = new StatusCheckVO();
        String content = null;
        
        try {
            if((content = streamReader(he.getRequestBody())) != null && !content.isEmpty()) {
                LOGGER.log(Level.INFO, "Subscibe received POST Json: {0}", content);
                he.sendResponseHeaders(subscribeUtil.subscribeFromJson(content, statusCheckVO) ? HttpURLConnection.HTTP_OK : HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
            } else 
                he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
        } catch (Exception e) {
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            statusCheckVO.setArray(null);
            statusCheckVO.setSubscribeHost(null);
            statusCheckVO.setCheck(false);
        } 
        he.close();

        // Updating Subscribers
        try {
            if (statusCheckVO.isCheck() && statusCheckVO.getArray() != null && statusCheckVO.getSubscribeHost() != null){
                if (content != null)
                    peerClient.subscribe(content);
                for(int i = 0; i < statusCheckVO.getArray() .size(); i++){
                    serviceClient.updateSubscriber(statusCheckVO.getArray().get(i).getAsJsonObject().get("name").getAsString(), statusCheckVO.getSubscribeHost());
                }
                
                // Saving state
                saveSubscriptionsState();
            }
        }catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     * HTTP POST handler
     * @param he
     * @throws IOException 
     */
    @Override
    public void doPut(HttpExchange he) throws IOException {
        StatusCheckVO statusCheckVO = new StatusCheckVO();
        String content = null;
        
        try {
            if((content = streamReader(he.getRequestBody())) != null && !content.isEmpty()) {
                LOGGER.log(Level.INFO, "Subscibe received PUT Json: {0}", content);
                he.sendResponseHeaders(subscribeUtil.unsubscribeFromJson(content, statusCheckVO) ? HttpURLConnection.HTTP_OK : HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
            } else 
                he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
        } catch (Exception e) {
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            statusCheckVO.setArray(null);
            statusCheckVO.setSubscribeHost(null);
            statusCheckVO.setCheck(false);
        } finally {
            subscribeMutex.release();
        }
        he.close();
        
        // Saving state
        if (statusCheckVO.isCheck() && statusCheckVO.getArray() != null && statusCheckVO.getSubscribeHost() != null) {
            if (content != null)
                peerClient.unsubscribe(content);
            saveSubscriptionsState();
        }
    }

    @Override
    public boolean getRunning() {
        return running;
    }

    @Override
    public Logger getLogger() {
        return LOGGER;
    }
}
