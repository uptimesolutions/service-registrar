/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.uptime.serviceRegistrar.http.handlers;
import com.sun.net.httpserver.HttpExchange;
import static com.uptime.serviceRegistrar.ServiceRegistrar.LOGGER;
import static com.uptime.serviceRegistrar.ServiceRegistrar.running;
import static com.uptime.serviceRegistrar.ServiceRegistrar.saveSubscriptionsState;
import com.uptime.serviceRegistrar.utils.CircuitBreakerUtil;
import com.uptime.serviceRegistrar.utils.PeerUtil;
import com.uptime.serviceRegistrar.utils.RegistrationUtil;
import com.uptime.serviceRegistrar.utils.SubscribeUtil;
import com.uptime.serviceRegistrar.vo.StatusCheckVO;
import com.uptime.services.http.handler.AbstractGenericHandler;
import static com.uptime.services.util.HttpUtils.parseQuery;
import static com.uptime.services.util.ServiceUtil.streamReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class PeerHandler extends AbstractGenericHandler {
    private final RegistrationUtil registrationUtils;
    private final CircuitBreakerUtil circuitBreakerUtils;
    private final SubscribeUtil subscribeUtils;
    private final PeerUtil peerUtils;

    /**
     * Constructor
     */
    public PeerHandler() {
        registrationUtils = new RegistrationUtil();
        circuitBreakerUtils = new CircuitBreakerUtil();
        subscribeUtils = new SubscribeUtil();
        peerUtils = new PeerUtil();
    }
    
    /**
     * HTTP POST handler
     * @param he
     * @throws IOException 
     */
    @Override
    public void doPost(HttpExchange he) throws IOException {
        StatusCheckVO statusCheckVO = new StatusCheckVO();
        Map<String, Object> params;
        String content;

        try {
            LOGGER.log(Level.INFO, "Peer received POST request: {0}", he.getRequestURI().getRawQuery());
            System.out.println("Peer received POST request: " + he.getRequestURI().getRawQuery());
            if ((params = parseQuery(he.getRequestURI().getRawQuery())) != null && params.get("action") != null) {
                if((content = streamReader(he.getRequestBody())) != null && !content.isEmpty()) {
                    LOGGER.log(Level.INFO, "Peer received POST Json: {0}", content);
                    System.out.println("Peer received POST Json: " + content);
                    switch (params.get("action").toString().toLowerCase()) {
                        case "connectioncheck":
                            he.sendResponseHeaders(peerUtils.peerConnectionCheck(content) ? HttpURLConnection.HTTP_OK : HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
                            break;
                        case "register":
                            he.sendResponseHeaders(registrationUtils.registerFromJson(content, statusCheckVO) ? HttpURLConnection.HTTP_OK : HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
                            break;
                        case "subscribe":
                            he.sendResponseHeaders(subscribeUtils.subscribeFromJson(content, statusCheckVO) ? HttpURLConnection.HTTP_OK : HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
                            if (statusCheckVO.isCheck() && statusCheckVO.getArray() != null && statusCheckVO.getSubscribeHost() != null)
                                saveSubscriptionsState();
                            break;
                        case "circuitbreaker":
                            he.sendResponseHeaders(circuitBreakerUtils.setStatuesCheckFromJson(content, statusCheckVO) ? HttpURLConnection.HTTP_OK : HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
                            if(statusCheckVO.getService() != null && statusCheckVO.getServiceHost() != null)
                                circuitBreakerUtils.setCircuitBreaker(statusCheckVO.getService(),statusCheckVO.getServiceHost(),true);
                            break;
                        default:
                            he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
                            break;
                    }
                } else 
                    he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
            } else 
                he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
        }catch(InterruptedException e) {
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }catch (Exception e) {
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        he.close();
    }
    
    /**
     * HTTP PUT handler
     * @param he
     * @throws IOException 
     */
    @Override
    public void doPut(HttpExchange he) throws IOException {
        StatusCheckVO statusCheckVO = new StatusCheckVO();
        Map<String, Object> params;
        String content;

        try {
            LOGGER.log(Level.INFO, "Peer received PUT request: {0}", he.getRequestURI().getRawQuery());
            System.out.println("Peer received PUT request: " + he.getRequestURI().getRawQuery());
            if ((params = parseQuery(he.getRequestURI().getRawQuery())) != null && params.get("action") != null) {
                if((content = streamReader(he.getRequestBody())) != null && !content.isEmpty()) {
                    LOGGER.log(Level.INFO, "Peer received PUT Json: {0}", content);
                    System.out.println("Peer received PUT Json: " + content);
                    switch (params.get("action").toString().toLowerCase()) {
                        case "unsubscribe":
                            he.sendResponseHeaders(subscribeUtils.unsubscribeFromJson(content, statusCheckVO) ? HttpURLConnection.HTTP_OK : HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
                            if (statusCheckVO.isCheck() && statusCheckVO.getArray() != null && statusCheckVO.getSubscribeHost() != null)
                                saveSubscriptionsState();
                            break;
                        default:
                            he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
                            break;
                    }
                } else 
                    he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
            } else 
                he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
        }catch(InterruptedException e) {
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }catch (Exception e) {
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        he.close();
    }

    /**
     * HTTP DELETE handler
     * @param he
     * @throws IOException 
     */
    @Override
    public void doDelete(HttpExchange he) throws IOException { 
        StatusCheckVO statusCheckVO = new StatusCheckVO();
        Map<String, Object> params;
                
        try{
            LOGGER.log(Level.INFO, "Peer received DELETE request: {0}", he.getRequestURI().getRawQuery());
            System.out.println("Peer received DELETE request: " + he.getRequestURI().getRawQuery());
            if ((params = parseQuery(he.getRequestURI().getRawQuery())) != null && params.get("action") != null) {
                switch (params.get("action").toString().toLowerCase()) {
                    case "unregister":
                        he.sendResponseHeaders(registrationUtils.unregisterFromParams(params, statusCheckVO) ? HttpURLConnection.HTTP_OK : HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
                        break;
                    case "circuitbreaker":
                        he.sendResponseHeaders(circuitBreakerUtils.setStatuesCheckFromParams(params, statusCheckVO) ? HttpURLConnection.HTTP_OK : HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
                        if(statusCheckVO.getService() != null && statusCheckVO.getServiceHost() != null) 
                            circuitBreakerUtils.setCircuitBreaker(statusCheckVO.getService(),statusCheckVO.getServiceHost(),false);
                        break;
                    default:
                        he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
                        break;
                }
            } else 
                he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
        }catch(InterruptedException e) {
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }catch (Exception e) {
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        he.close();
    }

    @Override
    public boolean getRunning() {
        return running;
    }

    @Override
    public Logger getLogger() {
        return LOGGER;
    }
}
