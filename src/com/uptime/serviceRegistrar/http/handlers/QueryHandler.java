/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.serviceRegistrar.http.handlers;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import static com.uptime.serviceRegistrar.ServiceRegistrar.LOGGER;
import static com.uptime.serviceRegistrar.ServiceRegistrar.running;
import com.uptime.serviceRegistrar.utils.QueryUtil;
import com.uptime.services.http.handler.AbstractGenericHandler;
import static com.uptime.services.util.HttpUtils.parseQuery;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ksimmons
 */
public class QueryHandler extends AbstractGenericHandler {
    private final QueryUtil queryUtil;
    
    /**
     * Constructor
     */
    public QueryHandler() {
        queryUtil = new QueryUtil();
    }
    
    /**
     * HTTP GET handler
     * @param he
     * @throws IOException 
     */
    @Override
    public void doGet(HttpExchange he) throws IOException {
        Map<String, Object> params;
        byte [] response;
        OutputStream os;
        String resp;
        Headers respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        
        try{
            LOGGER.log(Level.INFO, "Received GET request: {0}", he.getRequestURI().getRawQuery());
            params = parseQuery(he.getRequestURI().getRawQuery());
            if(params.isEmpty()){
                resp = "{\"outcome\":\"Hello World!\"}";
                response = resp.getBytes();
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            }else if(params.get("name") != null && !params.get("name").toString().isEmpty()){
                resp = queryUtil.queryJsonCreation(params.get("name").toString());
                response = resp.getBytes();
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            }else{
                resp = "{\"outcome\":\"Unknown Params\"}";
                response = resp.getBytes();
                he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_ACCEPTABLE, response.length);
            }
            LOGGER.log(Level.INFO, "Received GET response: {0}", resp);
        } catch (IllegalStateException e) {
            response = "{\"outcome\":\"IllegalStateException\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, response.length);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        } catch (NumberFormatException e) {
            response = "{\"outcome\":\"NumberFormatException\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, response.length);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        } catch (UnsupportedEncodingException e) {
            response = "{\"outcome\":\"UnsupportedEncodingException\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, response.length);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        } catch (Exception e) {
            response = "{\"outcome\":\"Exception\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, response.length);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();
    }

    @Override
    public boolean getRunning() {
        return running;
    }

    @Override
    public Logger getLogger() {
        return LOGGER;
    }
}
