/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.serviceRegistrar.utils;

import static com.uptime.serviceRegistrar.ServiceRegistrar.LOGGER;
import static com.uptime.serviceRegistrar.ServiceRegistrar.serviceHosts;
import static com.uptime.serviceRegistrar.ServiceRegistrar.serviceMutex;
import com.uptime.services.vo.ServiceHostVO;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

/**
 *
 * @author twilcox
 */
public class QueryUtil {
    
    /**
     * Creates a json of the usable hosts for a given service name
     * @param name, String object
     * @return String object
     */
    public String queryJsonCreation (String name) {
        StringBuilder sb;
        List<ServiceHostVO> hosts;
        ServiceHostVO host;
        
        try{
            LOGGER.log(Level.INFO, "Query Service Name: {0}", name);
            sb = new StringBuilder();
            sb  
                .append("{\"name\":\"").append(name).append("\",")
                .append("\"hosts\":[");
            serviceMutex.acquire();
            if((hosts = serviceHosts.get(name)) != null){
                hosts = hosts.stream().filter(h->h.isCircuitBreaker() == false).collect(Collectors.toList());
                for(int i = 0; i < hosts.size(); i++){
                    host = hosts.get(i);
                    sb
                        .append("{\"ipAddress\":\"").append(host.getIp()).append("\",")
                        .append("\"port\":\"").append(host.getPort()).append("\",")
                        .append("\"circuitBreaker\":\"").append("false").append(i < hosts.size() - 1 ? "\"}," : "\"}");
                }
            }
            sb.append("]}");
        } catch (Exception ex) {
            sb = new StringBuilder(); 
            sb
                .append("{\"name\":\"").append(name).append("\",")
                .append("{\"hosts\":[]}");
        } finally {
            serviceMutex.release();
        }
        return sb.toString();
    }
}
