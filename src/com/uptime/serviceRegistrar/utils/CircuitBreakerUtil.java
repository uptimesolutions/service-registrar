/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.serviceRegistrar.utils;

import com.uptime.serviceRegistrar.vo.StatusCheckVO;
import com.uptime.services.vo.ServiceHostVO;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import static com.uptime.serviceRegistrar.ServiceRegistrar.LOGGER;
import static com.uptime.serviceRegistrar.ServiceRegistrar.devOrPro;
import static com.uptime.serviceRegistrar.ServiceRegistrar.serviceHosts;
import static com.uptime.serviceRegistrar.ServiceRegistrar.serviceMutex;
import static com.uptime.services.util.ServiceUtil.portCheck;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;

/**
 *
 * @author twilcox
 */
public class CircuitBreakerUtil {
    
    /**
     * Set a StatusCheckVO from the given content
     * @param content, String object
     * @param statusCheckVO, StatusCheckVO object
     * @return boolean, true if success, else false
     * @throws Exception
     */
    public boolean setStatuesCheckFromJson (String content, StatusCheckVO statusCheckVO) throws Exception {
        JsonElement element;
        JsonObject json;
        
        if ((element = JsonParser.parseString(content)) != null && (json = element.getAsJsonObject()) != null && json.has("name") && json.has("ipAddress") && json.has("port") && 
                json.get("name") != null && !json.get("name").getAsString().isEmpty() && devOrPro(json.get("ipAddress").getAsString()) && portCheck(json.get("port").getAsInt())){
            statusCheckVO.setServiceHost(new ServiceHostVO());
            statusCheckVO.getServiceHost().setIp(json.get("ipAddress").getAsString());
            statusCheckVO.getServiceHost().setPort(json.get("port").getAsInt());
            statusCheckVO.setService(json.get("name").getAsString());
            return true;
        }
        return false;
    }
    
    /**
     * Set a StatusCheckVO from the given url params
     * @param params, Map of key = String objects, and value = objects
     * @param statusCheckVO, StatusCheckVO object
     * @return boolean, true if success, else false
     * @throws Exception
     */
    public boolean setStatuesCheckFromParams (Map<String, Object> params, StatusCheckVO statusCheckVO) throws Exception {
        if(params.get("name") != null && params.get("ipAddress") != null && params.get("port") != null && !params.get("name").toString().isEmpty() && devOrPro(params.get("ipAddress").toString()) && portCheck(params.get("port").toString())){
            statusCheckVO.setServiceHost(new ServiceHostVO());
            statusCheckVO.getServiceHost().setIp(params.get("ipAddress").toString());
            statusCheckVO.getServiceHost().setPort(Integer.parseInt(params.get("port").toString()));
            statusCheckVO.setService(params.get("name").toString());
            return true;
        }
        return false;
    }
    
    /**
     * update serviceHosts, set circuitbreaker, and update subscribed hosts
     * @param service, String object, the service name
     * @param host, ServiceHostVO object the ip address and port number of the effected service
     * @param circuitBreaker, boolean, true if "circuitbreaker" created and false if "circuitbreaker" cleared
     */
    public void setCircuitBreaker (String service, ServiceHostVO host, boolean circuitBreaker){
        ArrayList<ServiceHostVO> hosts;
        
        try{
            serviceMutex.acquire();
            if((hosts = serviceHosts.get(service)) != null) {
                hosts.stream().filter(h -> h.getIp().compareTo(host.getIp()) == 0 && h.getPort() == host.getPort()).forEachOrdered(h->{
                    h.setCircuitBreaker(circuitBreaker);
                    if(circuitBreaker){
                        LOGGER.log(Level.INFO, "CircuitBreaker added for: {0}, ip: {1}, port: {2}", new Object[]{service, h.getIp(), String.valueOf(h.getPort())});
                        System.out.println("CircuitBreaker added for: " + service + ", ip: " + h.getIp() + ", port: " + String.valueOf(h.getPort()));
                    } else {
                        LOGGER.log(Level.INFO, "CircuitBreaker cleared for: {0}, ip: {1}, port: {2}", new Object[]{service, h.getIp(), String.valueOf(h.getPort())});
                        System.out.println("CircuitBreaker cleared for: " + service + ", ip: " + h.getIp() + ", port: " + String.valueOf(h.getPort()));
                    }
                }); 
            }
        } catch(InterruptedException e){
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        } finally{
            serviceMutex.release();
        }
    }
}
