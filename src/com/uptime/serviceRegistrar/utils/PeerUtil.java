/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.serviceRegistrar.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import static com.uptime.serviceRegistrar.ServiceRegistrar.currentPeer;
import static com.uptime.serviceRegistrar.ServiceRegistrar.peerCheck;

/**
 *
 * @author twilcox
 */
public class PeerUtil {
    
    /**
     * Used the json content to check if the given host is this host
     * @param content, String object
     * @return boolean, true if the json is formated correctly, else false
     * @throws Exception 
     */
    public boolean peerConnectionCheck(String content) throws Exception {
        JsonElement element;
        JsonObject json;
        
        if ((element = JsonParser.parseString(content)) != null && (json = element.getAsJsonObject()) != null && json.has("peer")) {
            peerCheck = currentPeer != null && json.get("peer").getAsString().equals(currentPeer);
            return true;
        }
        return false;
    }
}
