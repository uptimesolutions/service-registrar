/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.serviceRegistrar.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import static com.uptime.serviceRegistrar.ServiceRegistrar.LOGGER;
import static com.uptime.serviceRegistrar.ServiceRegistrar.serviceHosts;
import static com.uptime.serviceRegistrar.ServiceRegistrar.serviceMutex;
import static com.uptime.serviceRegistrar.ServiceRegistrar.subscribeMutex;
import static com.uptime.serviceRegistrar.ServiceRegistrar.subscriptions;
import static com.uptime.serviceRegistrar.ServiceRegistrar.running;
import com.uptime.serviceRegistrar.vo.SubscribeHostVO;
import com.uptime.services.vo.ServiceHostVO;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 *
 * @author twilcox
 */
public class SystemUtil {
    
    /**
     * System commands sent by POST json
     * @param content, String object
     * @return boolean, true if success, else false;
     */
    public boolean postCommand (String content) {
        JsonElement element;
        JsonObject json;
        
        if ((element = JsonParser.parseString(content)) != null && (json = element.getAsJsonObject()) != null && json.has("serviceCommand")) {
            if(json.get("serviceCommand").getAsString().contains("shutdown")){
                running = false;
                return true;
            }
        }
        return false;
    }
    
    /**
     * System commands sent by PUT json
     * @param content, String object
     * @return boolean, true if success, else false;
     * @throws Exception
     */
    public boolean putCommand (String content) throws Exception {
        JsonElement element;
        JsonObject json;
        String name;
        
        if ((element = JsonParser.parseString(content)) != null && (json = element.getAsJsonObject()) != null && json.has("purge")) {
            switch (json.get("purge").getAsString().toLowerCase()){
                case "all":
                    try {
                        subscribeMutex.acquire();
                        serviceMutex.acquire();
                        serviceHosts.clear();
                        subscriptions.clear();
                    } finally {
                        subscribeMutex.release();
                        serviceMutex.release();
                    }
                    System.out.println("ServiceHosts and Subscriptions were purged.");
                    LOGGER.log(Level.INFO, "ServiceHosts and Subscriptions were purged.");
                    return true;
                case "services":
                    try {
                        serviceMutex.acquire();
                        serviceHosts.clear();
                    } finally {
                        serviceMutex.release();
                    }
                    System.out.println("ServiceHosts were purged.");
                    LOGGER.log(Level.INFO, "ServiceHosts were purged.");
                    return true;
                case "subscriptions":
                    if (json.has("service")) {
                        try {
                            if (!(name = json.get("service").getAsString()).isEmpty()) {
                                try{
                                    subscribeMutex.acquire();
                                    subscriptions.remove(name);
                                } finally {
                                    subscribeMutex.release();
                                }
                                System.out.println("Subscription:" + name + " was purged.");
                                LOGGER.log(Level.INFO, "Subscription:{0} was purged.", name);
                            }
                        } catch (Exception e) {
                            LOGGER.log(Level.SEVERE, e.getMessage(), e);
                            return false;
                        }
                    } else {
                        try {
                            subscribeMutex.acquire();
                            subscriptions.clear();
                        } finally {
                            subscribeMutex.release();
                        }
                        System.out.println("Subscriptions were purged.");
                        LOGGER.log(Level.INFO, "Subscriptions were purged.");
                    }
                    return true;
            }
        }
        return false;
    }
    
    /**
     * serviceHosts field to json 
     * @return String object
     */
    public String serviceHostsToJson () {
        StringBuilder json;
        List<String> keyList;
        List<ServiceHostVO> valueList;
        try {
            serviceMutex.acquire();
            json = new StringBuilder();
            json.append("{\"serviceHosts\":[");
            
            keyList = new ArrayList(serviceHosts.keySet());
            for(int i = 0; i < keyList.size(); i++) {
                json.append("{\"service\":\"").append(keyList.get(i)).append("\",\"hosts\":[");
                valueList = new ArrayList(serviceHosts.get(keyList.get(i)));
                for(int j = 0; j < valueList.size(); j++) {
                    json
                        .append("{\"ipAddress\":\"").append(valueList.get(j).getIp()).append("\",")
                        .append("\"circuitBreaker\":\"").append(valueList.get(j).isCircuitBreaker()).append("\",")
                        .append("\"port\":\"").append(valueList.get(j).getPort()).append(j < valueList.size() - 1 ? "\"}," : "\"}");
                }
                json.append(i < keyList.size() - 1 ? "]}," : "]}");
            }
            json.append("]}");
        } catch (Exception e) {
            json = new StringBuilder();
            json.append("{\"outcome\":\"").append(e.getMessage()).append("\"}");
        } finally {
            serviceMutex.release();
        }
        return json.toString();
    }
    
    /**
     * subscriptions field to json 
     * @return String object
     */
    public String subscriptionsToJson () {
        StringBuilder json;
        List<String> keyList;
        List<SubscribeHostVO> valueList;
        try {
            subscribeMutex.acquire();
            json = new StringBuilder();
            json.append("{\"subscriptions\":[");
            
            keyList = new ArrayList(subscriptions.keySet());
            for(int i = 0; i < keyList.size(); i++) {
                json.append("{\"service\":\"").append(keyList.get(i)).append("\",\"hosts\":[");
                valueList = new ArrayList(subscriptions.get(keyList.get(i)));
                for(int j = 0; j < valueList.size(); j++) {
                    json
                        .append("{\"ipAddress\":\"").append(valueList.get(j).getIpAddress()).append("\",")
                        .append("\"port\":\"").append(valueList.get(j).getPort()).append("\",")
                        .append("\"url\":\"").append(valueList.get(j).getUrl()).append("\",")
                        .append("\"source\":\"").append(valueList.get(j).getSource()).append(j < valueList.size() - 1 ? "\"}," : "\"}");
                }
                json.append(i < keyList.size() - 1 ? "]}," : "]}");
            }
            json.append("]}");
        } catch (Exception e) {
            json = new StringBuilder();
            json.append("{\"outcome\":\"").append(e.getMessage()).append("\"}");
        } finally {
            subscribeMutex.release();
        }
        return json.toString();
    }
}
