/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.serviceRegistrar.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import static com.uptime.serviceRegistrar.ServiceRegistrar.LOGGER;
import static com.uptime.serviceRegistrar.ServiceRegistrar.devOrPro;
import static com.uptime.serviceRegistrar.ServiceRegistrar.subscribeMutex;
import static com.uptime.serviceRegistrar.ServiceRegistrar.subscriptions;
import com.uptime.serviceRegistrar.vo.StatusCheckVO;
import com.uptime.serviceRegistrar.vo.SubscribeHostVO;
import static com.uptime.services.util.ServiceUtil.ipAddressCheck;
import static com.uptime.services.util.ServiceUtil.portCheck;
import static com.uptime.services.util.ServiceUtil.sourceCheck;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;

/**
 *
 * @author twilcox
 */
public class SubscribeUtil {
    
    /**
     * Subscribes from the given content
     * @param content, String object
     * @param statusCheckVO, StatusCheckVO object
     * @return boolean, true if success, else false
     * @throws Exception
     */
    public boolean subscribeFromJson (String content, StatusCheckVO statusCheckVO) throws Exception {
        String ipAddress, url, source;
        JsonElement element;
        JsonObject json;
        int port;

        if ((element = JsonParser.parseString(content)) != null && (json = element.getAsJsonObject()) != null && json.has("names") && json.has("ipAddress") && json.has("url") && json.has("port") && json.has("source")) {
            statusCheckVO.setArray(json.getAsJsonArray("names"));
            if(statusCheckVO.getArray() != null && statusCheckVO.getArray().size() > 0) {

                // Validates the indvidual elements of given array
                statusCheckVO.setCheck(true);
                for(JsonElement ele: statusCheckVO.getArray()){
                    try{
                        if(ele.getAsJsonObject().get("name") == null || ele.getAsJsonObject().get("name").getAsString().isEmpty()){  
                            statusCheckVO.setCheck(false);
                            break;
                        }
                    }catch (Exception ex) {
                        statusCheckVO.setCheck(false);
                        break;
                    }
                }

                // Validation checks
                statusCheckVO.setSubscribeHost(new SubscribeHostVO());
                statusCheckVO.getSubscribeHost().setIpAddress(ipAddressCheck(ipAddress = json.get("ipAddress").getAsString()) ? ipAddress : "");
                statusCheckVO.getSubscribeHost().setPort(portCheck(port = json.get("port").getAsInt()) ? port : 0);
                statusCheckVO.getSubscribeHost().setUrl(((url = json.get("url").getAsString()) != null && !url.isEmpty()) ? url : "");
                statusCheckVO.getSubscribeHost().setSource(sourceCheck(source = json.get("source").getAsString()) ? source : "");
                
                if(statusCheckVO.isCheck() && !statusCheckVO.getSubscribeHost().getIpAddress().isEmpty() && !statusCheckVO.getSubscribeHost().getSource().isEmpty()) {
                    if(devOrPro(ipAddress)){
                        for(int i = 0; i < statusCheckVO.getArray().size(); i++){
                            subscribeToService(statusCheckVO.getArray().get(i).getAsJsonObject().get("name").getAsString(), statusCheckVO.getSubscribeHost());
                        }
                        return true;
                    }
                } else if(statusCheckVO.isCheck() && !statusCheckVO.getSubscribeHost().getUrl().isEmpty() && !statusCheckVO.getSubscribeHost().getSource().isEmpty()) {
                    for(int i = 0; i < statusCheckVO.getArray().size(); i++){
                        subscribeToService(statusCheckVO.getArray().get(i).getAsJsonObject().get("name").getAsString(), statusCheckVO.getSubscribeHost());
                    }
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Unsubscribes from the given content
     * @param content, String object
     * @param statusCheckVO, StatusCheckVO object
     * @return boolean, true if success, else false
     * @throws Exception
     */
    public boolean unsubscribeFromJson (String content, StatusCheckVO statusCheckVO) throws Exception {
        String ipAddress, url, source;
        JsonElement element;
        JsonObject json;
        int port;
        
        if ((element = JsonParser.parseString(content)) != null && (json = element.getAsJsonObject()) != null && json.has("names") && json.has("ipAddress") && json.has("url") && json.has("port") && json.has("source")) {
            statusCheckVO.setArray(json.getAsJsonArray("names"));
            if(statusCheckVO.getArray() != null && statusCheckVO.getArray().size() > 0) {

                // Validates the indvidual elements of given array
                statusCheckVO.setCheck(true);
                for(JsonElement ele: statusCheckVO.getArray()){
                    try{
                        if(ele.getAsJsonObject().get("name") == null || ele.getAsJsonObject().get("name").getAsString().isEmpty()){    
                            statusCheckVO.setCheck(false);
                            break;
                        }
                    }catch (Exception ex) {
                        statusCheckVO.setCheck(false);
                        break;
                    }
                }

                // Validation checks
                statusCheckVO.setSubscribeHost(new SubscribeHostVO());
                statusCheckVO.getSubscribeHost().setIpAddress(ipAddressCheck(ipAddress = json.get("ipAddress").getAsString()) ? ipAddress : "");
                statusCheckVO.getSubscribeHost().setPort(portCheck(port = json.get("port").getAsInt()) ? port : 0);
                statusCheckVO.getSubscribeHost().setUrl(((url = json.get("url").getAsString()) != null && !url.isEmpty()) ? url : "");
                statusCheckVO.getSubscribeHost().setSource(sourceCheck(source = json.get("source").getAsString()) ? source : "");

                if(statusCheckVO.isCheck() && !statusCheckVO.getSubscribeHost().getIpAddress().isEmpty() && !statusCheckVO.getSubscribeHost().getSource().isEmpty()) {
                    if(devOrPro(ipAddress)){
                        for(int i = 0; i < statusCheckVO.getArray().size(); i++){
                            unsubscribeToService(statusCheckVO.getArray().get(i).getAsJsonObject().get("name").getAsString(), statusCheckVO.getSubscribeHost());
                        }
                        return true;
                    }
                } else if(statusCheckVO.isCheck() && !statusCheckVO.getSubscribeHost().getUrl().isEmpty() && !statusCheckVO.getSubscribeHost().getSource().isEmpty()){
                    for(int i = 0; i < statusCheckVO.getArray().size(); i++){
                        unsubscribeToService(statusCheckVO.getArray().get(i).getAsJsonObject().get("name").getAsString(), statusCheckVO.getSubscribeHost());
                    }
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Subscribe a Host to a given service
     * @param name, String object
     * @param host, SubscribeHostVO object
     * @throws Exception 
     */
    private void subscribeToService (String name, SubscribeHostVO host) throws Exception {
        ArrayList<SubscribeHostVO> list;
        
        try {
            subscribeMutex.acquire();     
            if (subscriptions.containsKey(name)){
                if(!subscriptions.get(name).stream().anyMatch(s->s.getIpAddress().equalsIgnoreCase(host.getIpAddress()) && s.getPort() == host.getPort() && s.getUrl().equalsIgnoreCase(host.getUrl()) && s.getSource().equalsIgnoreCase(host.getSource()))) {
                    subscriptions.get(name).add(host);
                    LOGGER.log(Level.INFO, "Subscribing to: {0}, from ip: {1}, port: {2}, url: {3}, source: {4}", new Object[]{name, host.getIpAddress(), String.valueOf(host.getPort()), host.getUrl(), host.getSource()});
                    System.out.println("Subscribing to: " + name + ", from ip: " + host.getIpAddress() + ", port: " + String.valueOf(host.getPort()) + ", url: " + host.getUrl() + ", source: " + host.getSource());
                }
            } else {
                list = new ArrayList();
                list.add(host);
                subscriptions.put(name,list);
                LOGGER.log(Level.INFO, "Subscribing to: {0}, from ip: {1}, port: {2}, url: {3}, source: {4}", new Object[]{name, host.getIpAddress(), String.valueOf(host.getPort()), host.getUrl(), host.getSource()});
                System.out.println("Subscribing to: " + name + ", from ip: " + host.getIpAddress() + ", port: " + String.valueOf(host.getPort()) + ", url: " + host.getUrl() + ", source: " + host.getSource());
            }
        }finally {
            subscribeMutex.release();
        }
    }
    
    /**
     * Unsubscribe a Host from a given service
     * @param name, String object
     * @param host, SubscribeHostVO object
     * @throws Exception 
     */
    private void unsubscribeToService (String name, SubscribeHostVO host) throws Exception {
        ArrayList<SubscribeHostVO> list;
        SubscribeHostVO currentHost;
        boolean removed = false;
        try{
            subscribeMutex.acquire();
            if ((list = subscriptions.get(name)) != null) {
                for(Iterator<SubscribeHostVO> i = list.iterator(); i.hasNext();) {
                    if ((currentHost = i.next()) != null) {
                        if(currentHost.getIpAddress().equalsIgnoreCase(host.getIpAddress()) && currentHost.getPort() == host.getPort() && currentHost.getUrl().equalsIgnoreCase(host.getUrl()) && currentHost.getSource().equalsIgnoreCase(host.getSource())) {
                            i.remove();
                            removed = true;
                            break;
                        }
                    } 
                }
                
                if (removed) {
                    LOGGER.log(Level.INFO, "Unsubscribing from: {0}, from ip: {1}, port: {2}, url: {3}, source: {4}", new Object[]{name, host.getIpAddress(), String.valueOf(host.getPort()), host.getUrl(), host.getSource()});
                    System.out.println("Unsubscribing from: " + name + ", from ip: " + host.getIpAddress() + ", port: " + String.valueOf(host.getPort()) + ", url: " + host.getUrl() + ", source: " + host.getSource());
                    subscriptions.replace(name, list);
                }
            }
        } finally {
            subscribeMutex.release();
        }
    }
}
