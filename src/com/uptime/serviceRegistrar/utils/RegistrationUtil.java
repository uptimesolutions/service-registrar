/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.serviceRegistrar.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import static com.uptime.serviceRegistrar.ServiceRegistrar.LOGGER;
import static com.uptime.serviceRegistrar.ServiceRegistrar.devOrPro;
import static com.uptime.serviceRegistrar.ServiceRegistrar.serviceHosts;
import static com.uptime.serviceRegistrar.ServiceRegistrar.serviceMutex;
import com.uptime.serviceRegistrar.vo.StatusCheckVO;
import com.uptime.services.vo.ServiceHostVO;
import static com.uptime.services.util.ServiceUtil.portCheck;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;

/**
 *
 * @author twilcox
 */
public class RegistrationUtil {
    
    /**
     * Register from the given content
     * @param content, String object
     * @param statusCheckVO, StatusCheckVO object
     * @return boolean, true if success, else false
     * @throws InterruptedException
     * @throws Exception
     */
    public boolean registerFromJson (String content, StatusCheckVO statusCheckVO) throws InterruptedException, Exception {
        ArrayList<ServiceHostVO> hosts;
        JsonElement element;
        JsonObject json;
        
        if ((element = JsonParser.parseString(content)) != null && (json = element.getAsJsonObject()) != null && json.has("name") && json.has("ipAddress") && json.has("port")) {
            // Note: if this is set to production then only IPs starting with 10.1.30 will work. All others will receive a response code of 406
            if(json.get("name") != null && !json.get("name").getAsString().isEmpty() && devOrPro(json.get("ipAddress").getAsString()) && portCheck(json.get("port").getAsInt())){
                statusCheckVO.setServiceHost(new ServiceHostVO());
                statusCheckVO.getServiceHost().setIp(json.get("ipAddress").getAsString());
                statusCheckVO.getServiceHost().setPort(json.get("port").getAsInt());
                if (json.has("circuitBreaker")) {
                    try {
                        statusCheckVO.getServiceHost().setCircuitBreaker(json.get("circuitBreaker").getAsBoolean());
                    }catch (Exception ex) {}
                }
                statusCheckVO.setService(json.get("name").getAsString());

                // Add new host to list if not found
                try{
                    serviceMutex.acquire();
                    if((hosts = serviceHosts.get(statusCheckVO.getService())) != null){
                        for(ServiceHostVO h : hosts){
                            if(h.getIp().compareTo(statusCheckVO.getServiceHost().getIp()) == 0 && h.getPort() == statusCheckVO.getServiceHost().getPort()) {
                                statusCheckVO.setCheck(true);
                                break;
                            }
                        }
                        if(!statusCheckVO.isCheck()){
                            hosts.add(statusCheckVO.getServiceHost());
                            serviceHosts.put(statusCheckVO.getService(), hosts);
                            LOGGER.log(Level.INFO, "Registering name: {0}, ip: {1}, port: {2}", new Object[]{statusCheckVO.getService(), statusCheckVO.getServiceHost().getIp(), String.valueOf(statusCheckVO.getServiceHost().getPort())});
                            System.out.println("Registering name: " + statusCheckVO.getService() + ", ip: " + statusCheckVO.getServiceHost().getIp() + ", port: " + String.valueOf(statusCheckVO.getServiceHost().getPort()));
                        }
                    } else {
                        hosts = new ArrayList();
                        hosts.add(statusCheckVO.getServiceHost());
                        serviceHosts.put(statusCheckVO.getService(), hosts);
                        LOGGER.log(Level.INFO, "Registering name: {0}, ip: {1}, port: {2}", new Object[]{statusCheckVO.getService(), statusCheckVO.getServiceHost().getIp(), String.valueOf(statusCheckVO.getServiceHost().getPort())});
                        System.out.println("Registering name: " + statusCheckVO.getService() + ", ip: " + statusCheckVO.getServiceHost().getIp() + ", port: " + String.valueOf(statusCheckVO.getServiceHost().getPort()));
                    }
                    return true;
                } finally {
                    serviceMutex.release();
                }
            }
        }
        return false;
    }
    
    /**
     * Unregister host from the given url params
     * @param params, Map of key = String objects, and value = objects
     * @param statusCheckVO, StatusCheckVO object
     * @return boolean, true if success, else false
     * @throws InterruptedException
     * @throws Exception
     */
    public boolean unregisterFromParams (Map<String, Object> params, StatusCheckVO statusCheckVO) throws InterruptedException, Exception {
        ArrayList<ServiceHostVO> hosts;
        ServiceHostVO host;
        
        if(params.get("name") != null && !params.get("name").toString().isEmpty() && devOrPro(params.get("ipAddress").toString()) && portCheck(params.get("port").toString())){
            try{
                statusCheckVO.setService(params.get("name").toString());

                serviceMutex.acquire();
                if((hosts = serviceHosts.get(statusCheckVO.getService())) != null){
                    for(int i = 0; i < hosts.size(); i++){
                        host = hosts.get(i);
                        if(host.getIp().compareTo(params.get("ipAddress").toString()) == 0 && host.getPort() == Integer.parseInt(params.get("port").toString())){
                            statusCheckVO.setCheck(true);
                            statusCheckVO.setServiceHost(host);
                            hosts.remove(i);
                            serviceHosts.put(statusCheckVO.getService(), hosts);
                            LOGGER.log(Level.INFO, "Unregistering name: {0}, ip: {1}, port: {2}", new Object[]{statusCheckVO.getService(), host.getIp(), String.valueOf(host.getPort())});
                            System.out.println("Unregistering name: " + statusCheckVO.getService() + ", ip: " + host.getIp() + ", port: " + String.valueOf(host.getPort()));
                            break;
                        }
                    }
                }
            } finally{
                serviceMutex.release();
            }
            return true;
        }
        return false;
        
    }
}
