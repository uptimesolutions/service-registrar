/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.serviceRegistrar.threads;

import static com.uptime.serviceRegistrar.ServiceRegistrar.LOGGER;
import static com.uptime.serviceRegistrar.ServiceRegistrar.saveServiceHostState;
import static com.uptime.serviceRegistrar.ServiceRegistrar.serviceHosts;
import static com.uptime.serviceRegistrar.ServiceRegistrar.serviceMutex;
import com.uptime.serviceRegistrar.http.client.ServiceClient;
import com.uptime.services.vo.ServiceHostVO;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;

/**
 *
 * @author ksimmons
 */
public class MonitorThread implements Runnable {
    private final List<HostTestThread> hostTestList;
    private final AtomicLong lastProcessingTime;
    private final Long MAX_PROCESSING_TIME;
    private final AtomicBoolean running, stop;
    private final int SLEEP_TIME; 
    private Thread worker;
    ServiceClient serviceClient;
    
    /**
     * Constructor
     */
    public MonitorThread() {
        stop = new AtomicBoolean(true);
        running = new AtomicBoolean(false);
        serviceClient = new ServiceClient();
        hostTestList = new ArrayList();
        lastProcessingTime = new AtomicLong(System.currentTimeMillis());
        MAX_PROCESSING_TIME = 1200000L;
        SLEEP_TIME = 120000; // 2 min
    }
    
    /**
     * Starts the worker thread
     */
    public void start() {
        stop.set(false);
        worker = new Thread(this);
        worker.start();
    }
    
    /**
     * interrupts the worker thread
     */
    public void interrupt() {
        running.set(false);
        worker.interrupt();
        hostTestList.forEach(thread -> thread.interrupt());
    }
    
    /**
     * @return the stop
     */
    public boolean isStopped() {
        return stop.get();
    }
    
    /**
     * @return the running
     */
    public boolean isRunning() {
        return running.get();
    }
    
    /**
     * Checks if the worker thread is hung up
     * @return boolean, true is hung up, false otherwise
     */
    public boolean isHung() {
        return (System.currentTimeMillis() - lastProcessingTime.get() > MAX_PROCESSING_TIME);
    }
    
    /**
     * Runs worker thread.
     * Monitors Hosts
     */
    @Override
    public void run() {
        HostTestThread hostTestThread;
        List<String> services;
        boolean newCircuitBreaker;
        
        running.set(true);
        while(running.get()){
            try{
                lastProcessingTime.set(System.currentTimeMillis());
                
                // Check statis of HostTestThreads
                if(running.get()){
                    try {
                        for (Iterator<HostTestThread> iterator = hostTestList.iterator(); iterator.hasNext();) {
                            if(!running.get()) break;
                            hostTestThread = iterator.next();
                            if (hostTestThread.isStopped() && hostTestThread.isDispose())
                                iterator.remove();
                            else if (hostTestThread.isStopped() && !hostTestThread.isDispose())
                                hostTestThread.start();
                            else if (hostTestThread.isHung())
                                hostTestThread.interrupt();
                        }
                    } catch (Exception e) {
                        LOGGER.log(Level.SEVERE, e.getMessage(), e);
                    }
                }
                
                // Check activity of all hosts in serviceHosts
                services = new ArrayList();
                if(running.get()){
                    try {
                        serviceMutex.acquire();
                        System.out.println("Start MoritorThread loop...");
                        
                        for (String key : serviceHosts.keySet()){
                            if(!running.get()) break;
                            for(ServiceHostVO host: serviceHosts.get(key)){
                                if(!running.get()) break;
                                if (!host.isCircuitBreaker() && !serviceClient.connectivityCheck(host)) {
                                    services.add(key);
                                    host.setCircuitBreaker(true);
                                    hostTestThread = new HostTestThread(host, key);
                                    LOGGER.log(Level.INFO, "HostTestThread Starting for, Service: {0}, Ip Address: {1}, Port: {2}", new Object[]{key, host.getIp(), String.valueOf(host.getPort())});
                                    System.out.println("HostTestThread Starting for, Service: " + key + ", Ip Address: " + host.getIp() + ", Port: " + String.valueOf(host.getPort()));
                                    hostTestThread.start();
                                    hostTestList.add(hostTestThread);
                                } else if (host.isCircuitBreaker()) {
                                    newCircuitBreaker = true;
                                    for (HostTestThread thread : hostTestList) {
                                        if (thread.hostCheck(host, key)) {
                                            newCircuitBreaker = false;
                                            break;
                                        }
                                    }
                                    if (newCircuitBreaker) {
                                        services.add(key);
                                        hostTestThread = new HostTestThread(host, key);
                                        LOGGER.log(Level.INFO, "HostTestThread Starting for, Service: {0}, Ip Address: {1}, Port: {2}", new Object[]{key, host.getIp(), String.valueOf(host.getPort())});
                                        System.out.println("HostTestThread Starting for, Service: " + key + ", Ip Address: " + host.getIp() + ", Port: " + String.valueOf(host.getPort()));
                                        hostTestThread.start();
                                        hostTestList.add(hostTestThread);
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        LOGGER.log(Level.SEVERE, e.getMessage(), e);
                    } finally {
                        serviceMutex.release();
                    }
                } 
                
                /**
                 * Update Subscribers
                 */
                if(running.get()){
                    for (String key : services){
                        if(!running.get()) break;
                        serviceClient.updateSubscribers(key);
                    }
                }
                
                // Save State and Sleep
                if(running.get()){
                    System.out.println("MoritorThread Sleeping...");
                    Thread.sleep(SLEEP_TIME);
                    saveServiceHostState();
                }
            } catch(InterruptedException e){
            } catch(Exception e){
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
            }
        }
        LOGGER.log(Level.WARNING, "MoritorThread Stopped.");
        System.out.println("MoritorThread Stopped.");
        
        stop.set(true);
    }
}
