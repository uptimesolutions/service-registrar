/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.serviceRegistrar.threads;

import static com.uptime.serviceRegistrar.ServiceRegistrar.LOGGER;
import static com.uptime.serviceRegistrar.ServiceRegistrar.saveServiceHostState;
import static com.uptime.serviceRegistrar.ServiceRegistrar.serviceHosts;
import static com.uptime.serviceRegistrar.ServiceRegistrar.serviceMutex;
import com.uptime.serviceRegistrar.http.client.ServiceClient;
import com.uptime.services.vo.ServiceHostVO;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;

/**
 *
 * @author ksimmons
 */
public class HostTestThread implements Runnable {
    private final AtomicBoolean running, stop, dispose;
    private final ServiceClient serviceClient;
    private final AtomicLong lastProcessingTime;
    private final Long MAX_PROCESSING_TIME;
    private final ServiceHostVO HOST;
    private final int SLEEP_TIME; 
    private final String SERVICE;
    private Thread worker;
    
    /**
     * Constructor
     * @param host, ServiceHostVO object
     * @param service, String object
     */
    public HostTestThread(ServiceHostVO host, String service) {
        stop = new AtomicBoolean(true);
        running = new AtomicBoolean(false);
        dispose = new AtomicBoolean(false); 
        serviceClient = new ServiceClient();
        lastProcessingTime = new AtomicLong(System.currentTimeMillis());
        MAX_PROCESSING_TIME = 120000L;
        SLEEP_TIME = 10000;
        SERVICE = service;
        HOST = host;
    }
    
    /**
     * Starts the worker thread
     */
    public void start() {
        stop.set(false);
        worker = new Thread(this);
        worker.start();
    }
    
    /**
     * interrupts the worker thread
     */
    public void interrupt() {
        running.set(false);
        worker.interrupt();
    }
    
    /**
     * @return the stop
     */
    public boolean isStopped() {
        return stop.get();
    }
    
    /**
     * @return the dispose
     */
    public boolean isDispose() {
        return dispose.get();
    }
    
    /**
     * Checks if the worker thread is hung up
     * @return boolean, true is hung up, false otherwise
     */
    public boolean isHung() {
        return (System.currentTimeMillis() - lastProcessingTime.get() > MAX_PROCESSING_TIME);
    }
    
    /**
     * Compares the given host and service with the service and host being checked
     * @param host, ServiceHostVO object
     * @param service, String object
     * @return boolean, true if host and service match, else false
     */
    public boolean hostCheck (ServiceHostVO host, String service) {
        return service.equals(SERVICE) && host.getIp().equals(HOST.getIp()) && host.getPort() == HOST.getPort();
    }
    
    /**
     * Runs worker thread.
     * Testing given host
     */
    @Override
    public void run() {
        boolean notify = false;
        ServiceHostVO current;
        int count = 0;
        
        running.set(true);
        while(running.get() && !dispose.get()){
            try{
                lastProcessingTime.set(System.currentTimeMillis());
                
                // Set CircuitBreaker
                if(running.get() && serviceClient.connectivityCheck(HOST)){
                    notify = false;
                    try {
                        serviceMutex.acquire();
                        serviceHosts.get(SERVICE).stream().filter(host -> host.getIp().equals(HOST.getIp()) && host.getPort() == HOST.getPort()).forEach(host -> host.setCircuitBreaker(false));
                        LOGGER.log(Level.INFO, "Service: {0}, Ip Address: {1}, Port: {2}, Circuit Breaker cleared.", new Object[]{SERVICE, HOST.getIp(), String.valueOf(HOST.getPort())});
                        System.out.println("Service: " + SERVICE + ", Ip Address: " + HOST.getIp() + ", Port: " + String.valueOf(HOST.getPort()) + ", Circuit Breaker cleared.");
                        dispose.set(true);
                        notify = true;
                    } finally {
                        serviceMutex.release();
                    }
                } 
                
                // Send Updates to subscribers
                if(running.get() && notify)
                    serviceClient.updateSubscribers(SERVICE);
                
                // Remove the host
                if(running.get() && count >= 10){
                    try {
                        serviceMutex.acquire();
                        for (Iterator<ServiceHostVO> iterator = serviceHosts.get(SERVICE).iterator(); iterator.hasNext();) {
                            if(!running.get()) break;
                            current = iterator.next();
                            if (current.getIp().equals(HOST.getIp()) && current.getPort() == HOST.getPort()) {
                                LOGGER.log(Level.INFO, "Service: {0}, Ip Address: {1}, Port: {2}, Removed from service host list.", new Object[]{SERVICE, HOST.getIp(), String.valueOf(HOST.getPort())});
                                System.out.println("Service: " + SERVICE + ", Ip Address: " + HOST.getIp() + ", Port: " + String.valueOf(HOST.getPort()) + ", Removed from service host list.");
                                iterator.remove();
                                dispose.set(true);
                                break;
                            }
                        }
                    } finally {
                        serviceMutex.release();
                    }
                }
                
                // Save State and Sleep
                if(running.get()){
                    count++;
                    Thread.sleep(SLEEP_TIME);
                }
            } catch(InterruptedException e){
            } catch(Exception e){
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
            }
        }
        LOGGER.log(Level.INFO, "HostTestThread Stopped for, Service: {0}, Ip Address: {1}, Port: {2}", new Object[]{SERVICE, HOST.getIp(), String.valueOf(HOST.getPort())});
        System.out.println("HostTestThread Stopped for, Service: " + SERVICE + ", Ip Address: " + HOST.getIp() + ", Port: " + String.valueOf(HOST.getPort()));
        saveServiceHostState();
        stop.set(true);
    }
}
