/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.serviceRegistrar.threads;

import static com.uptime.serviceRegistrar.ServiceRegistrar.LOGGER;
import static com.uptime.serviceRegistrar.ServiceRegistrar.firstRun;
import static com.uptime.serviceRegistrar.ServiceRegistrar.peerUrls;
import static com.uptime.serviceRegistrar.ServiceRegistrar.readSerialized;
import static com.uptime.serviceRegistrar.ServiceRegistrar.saveServiceHostState;
import static com.uptime.serviceRegistrar.ServiceRegistrar.saveSubscriptionsState;
import static com.uptime.serviceRegistrar.ServiceRegistrar.serviceHosts;
import static com.uptime.serviceRegistrar.ServiceRegistrar.serviceMutex;
import static com.uptime.serviceRegistrar.ServiceRegistrar.subscribeMutex;
import static com.uptime.serviceRegistrar.ServiceRegistrar.subscriptions;
import com.uptime.serviceRegistrar.http.client.PeerClient;
import com.uptime.serviceRegistrar.vo.PeerVO;
import com.uptime.serviceRegistrar.vo.SubscribeHostVO;
import com.uptime.services.vo.ServiceHostVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;

/**
 *
 * @author twilcox
 */
public class PeerSyncThread implements Runnable {
    private final AtomicLong lastProcessingTime;
    private final Long MAX_PROCESSING_TIME;
    private final AtomicBoolean running, stop, sleeping;
    private final int SLEEP_TIME; 
    private Thread worker;
    PeerClient peerClient;

    /**
     * Constructor
     */
    public PeerSyncThread() {
        stop = new AtomicBoolean(true);
        sleeping = new AtomicBoolean(true);
        running = new AtomicBoolean(false);
        peerClient = new PeerClient();
        lastProcessingTime = new AtomicLong(System.currentTimeMillis());
        MAX_PROCESSING_TIME = 3600000L;
        SLEEP_TIME = 1800000; // 30 min
    }

    /**
     * Starts the worker thread
     */
    public void start() {
        stop.set(false);
        sleeping.set(false);
        worker = new Thread(this);
        worker.start();
    }
    
    /**
     * interrupts the worker thread
     */
    public void interrupt() {
        running.set(false);
        sleeping.set(true);
        worker.interrupt();
    }
    
    /**
     * @return the stop
     */
    public boolean isStopped() {
        return stop.get();
    }
    
    /**
     * @return the running
     */
    public boolean isRunning() {
        return running.get();
    }
    
    /**
     * @return the sleeping
     */
    public boolean isSleeping() {
        return sleeping.get();
    }
    
    /**
     * Checks if the worker thread is hung up
     * @return boolean, true is hung up, false otherwise
     */
    public boolean isHung() {
        return (System.currentTimeMillis() - lastProcessingTime.get() > MAX_PROCESSING_TIME);
    }
    
    /**
     * Runs worker thread.
     * Monitors Hosts
     */
    @Override
    public void run() {
        running.set(true);
        PeerVO pvo;
        List<PeerVO> pvoList;
        ArrayList<ServiceHostVO> serviceHostList;
        ArrayList<SubscribeHostVO> subscribeHostVOList;
        Map<String,ArrayList<ServiceHostVO>> currentServiceHosts;
        Map<String,ArrayList<SubscribeHostVO>> currentSubscriptions;
        ServiceHostVO newServiceHost;
        SubscribeHostVO newSubscribeHostVO;
        
        while(running.get()){
            try{
                lastProcessingTime.set(System.currentTimeMillis());
                
                pvoList = new ArrayList();
                for (String peer : peerUrls) {
                    if(!running.get()) break;
                    try {
                        pvo = new PeerVO();
                        pvo.setHost(peer);
                        pvo.setCurrentServiceHosts(peerClient.getServiceHosts(peer));
                        pvo.setCurrentSubscriptions(peerClient.getSubscriptions(peer));
                        if(pvo.getCurrentServiceHosts() != null && pvo.getCurrentSubscriptions() != null)
                            pvoList.add(pvo);
                    }catch (Exception e) {
                        System.out.println("Skipping Peer: " + peer);
                        LOGGER.log(Level.WARNING, "Skipping Peer: {0}", peer);
                    }
                }
                
                // Syncing the fields subscriptions and serviceHosts of all peers
                if (!pvoList.isEmpty()) {
                    firstRun = false;
                    currentServiceHosts = new HashMap();
                    currentSubscriptions =  new HashMap();
                    
                    for(PeerVO peerVO : pvoList) {
                        if(!running.get()) break;
                        for (String key : peerVO.getCurrentServiceHosts().keySet()) {
                            if(!running.get()) break;
                            serviceHostList = new ArrayList(peerVO.getCurrentServiceHosts().get(key));
                            if (!currentServiceHosts.containsKey(key))
                                currentServiceHosts.put(key, serviceHostList);
                            else {
                                for (ServiceHostVO serviceHost : serviceHostList) {
                                    if(!running.get()) break;
                                    if (!currentServiceHosts.get(key).stream().anyMatch(sh -> sh.getIp().equals(serviceHost.getIp()) && sh.getPort() == serviceHost.getPort())) { 
                                        newServiceHost = new ServiceHostVO();
                                        newServiceHost.setIp(serviceHost.getIp());
                                        newServiceHost.setPort(serviceHost.getPort());
                                        newServiceHost.setCircuitBreaker(serviceHost.isCircuitBreaker());
                                        currentServiceHosts.get(key).add(newServiceHost);
                                    }
                                }
                            }
                        }
                        
                        for (String key : peerVO.getCurrentSubscriptions().keySet()) {
                            if(!running.get()) break;
                            subscribeHostVOList = new ArrayList(peerVO.getCurrentSubscriptions().get(key));
                            if (!currentSubscriptions.containsKey(key))
                                currentSubscriptions.put(key, subscribeHostVOList);
                            else {
                                for (SubscribeHostVO subscribeHostVO : subscribeHostVOList) {
                                    if(!running.get()) break;
                                    if (!currentSubscriptions.get(key).stream().anyMatch(sh -> sh.getUrl().equals(subscribeHostVO.getUrl()) && sh.getSource().equals(subscribeHostVO.getSource()) && sh.getIpAddress().equals(subscribeHostVO.getIpAddress()) && sh.getPort() == subscribeHostVO.getPort())) {
                                        newSubscribeHostVO = new SubscribeHostVO();
                                        newSubscribeHostVO.setIpAddress(subscribeHostVO.getIpAddress());
                                        newSubscribeHostVO.setPort(subscribeHostVO.getPort());
                                        newSubscribeHostVO.setSource(subscribeHostVO.getSource());
                                        newSubscribeHostVO.setUrl(subscribeHostVO.getUrl());
                                        currentSubscriptions.get(key).add(newSubscribeHostVO);
                                    }
                                }
                            }
                        }
                    }
                    
                    // Repopulating the fields subscriptions and serviceHosts
                    if(running.get()){
                        try {
                            serviceMutex.acquire(); 
                            for (String key : serviceHosts.keySet()) {
                                serviceHostList = new ArrayList(serviceHosts.get(key));
                                if (!currentServiceHosts.containsKey(key))
                                    currentServiceHosts.put(key, serviceHostList);
                                else {
                                    for (ServiceHostVO serviceHost : serviceHostList) {
                                        if (!currentServiceHosts.get(key).stream().anyMatch(sh -> sh.getIp().equals(serviceHost.getIp()) && sh.getPort() == serviceHost.getPort())) { 
                                            newServiceHost = new ServiceHostVO();
                                            newServiceHost.setIp(serviceHost.getIp());
                                            newServiceHost.setPort(serviceHost.getPort());
                                            newServiceHost.setCircuitBreaker(serviceHost.isCircuitBreaker());
                                            currentServiceHosts.get(key).add(newServiceHost);
                                        }
                                    }
                                }
                            }
                            serviceHosts.clear();
                            serviceHosts.putAll(currentServiceHosts);
                        }finally {
                            serviceMutex.release();
                        }
                        try {
                            subscribeMutex.acquire(); 
                            for (String key : subscriptions.keySet()) {
                                subscribeHostVOList = new ArrayList(subscriptions.get(key));
                                if (!currentSubscriptions.containsKey(key))
                                    currentSubscriptions.put(key, subscribeHostVOList);
                                else {
                                    for (SubscribeHostVO subscribeHostVO : subscribeHostVOList) {
                                        if (!currentSubscriptions.get(key).stream().anyMatch(sh -> sh.getUrl().equals(subscribeHostVO.getUrl()) && sh.getSource().equals(subscribeHostVO.getSource()) && sh.getIpAddress().equals(subscribeHostVO.getIpAddress()) && sh.getPort() == subscribeHostVO.getPort())) {
                                            currentSubscriptions.get(key).add(subscribeHostVO);
                                        }
                                    }
                                }
                            }
                            subscriptions.clear();
                            subscriptions.putAll(currentSubscriptions);
                        }finally {
                            subscribeMutex.release();
                        }
                    }
                    
                    // Set the values for the missing subscriptions and serviceHosts for the peers
                    if(running.get()){
                        for(PeerVO peerVO : pvoList) {
                            if(!running.get()) break;
                            peerVO.setMissingServiceHosts(new HashMap());
                            for (String key : currentServiceHosts.keySet()) {
                                if(!running.get()) break;
                                serviceHostList = new ArrayList(currentServiceHosts.get(key));
                                if (!peerVO.getCurrentServiceHosts().containsKey(key))
                                    peerVO.getMissingServiceHosts().put(key, serviceHostList);
                                else {
                                    for (ServiceHostVO serviceHost : serviceHostList) {
                                        if(!running.get()) break;
                                        if (!peerVO.getCurrentServiceHosts().get(key).stream().anyMatch(sh -> sh.getIp().equals(serviceHost.getIp()) && sh.getPort() == serviceHost.getPort())) { 
                                            if(!peerVO.getMissingServiceHosts().containsKey(key))
                                                peerVO.getMissingServiceHosts().put(key, new ArrayList());
                                            newServiceHost = new ServiceHostVO();
                                            newServiceHost.setIp(serviceHost.getIp());
                                            newServiceHost.setPort(serviceHost.getPort());
                                            newServiceHost.setCircuitBreaker(serviceHost.isCircuitBreaker());
                                            peerVO.getMissingServiceHosts().get(key).add(newServiceHost);
                                        }
                                    }
                                }
                            }
                            
                            if(!running.get()) break;
                            peerVO.setMissingSubscriptions(new HashMap());
                            for (String key : currentSubscriptions.keySet()) {
                                if(!running.get()) break;
                                subscribeHostVOList = new ArrayList(currentSubscriptions.get(key));
                                if (!peerVO.getCurrentSubscriptions().containsKey(key))
                                    peerVO.getMissingSubscriptions().put(key, subscribeHostVOList);
                                else {
                                    for (SubscribeHostVO subscribeHostVO : subscribeHostVOList) {
                                        if(!running.get()) break;
                                        if (!peerVO.getCurrentSubscriptions().get(key).stream().anyMatch(sh -> sh.getUrl().equals(subscribeHostVO.getUrl()) && sh.getSource().equals(subscribeHostVO.getSource()) && sh.getIpAddress().equals(subscribeHostVO.getIpAddress()) && sh.getPort() == subscribeHostVO.getPort())) {
                                            if(!peerVO.getCurrentSubscriptions().containsKey(key))
                                                peerVO.getCurrentSubscriptions().put(key, new ArrayList());
                                            newSubscribeHostVO = new SubscribeHostVO();
                                            newSubscribeHostVO.setIpAddress(subscribeHostVO.getIpAddress());
                                            newSubscribeHostVO.setPort(subscribeHostVO.getPort());
                                            newSubscribeHostVO.setSource(subscribeHostVO.getSource());
                                            newSubscribeHostVO.setUrl(subscribeHostVO.getUrl());
                                            peerVO.getMissingSubscriptions().get(key).add(newSubscribeHostVO);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    // POST missing serviceHosts and subscriptions to peers
                    if(running.get()){
                        for(PeerVO peerVO : pvoList) {
                            if(!running.get()) break;
                            if(peerVO.getMissingServiceHosts() != null && !peerVO.getMissingServiceHosts().isEmpty())
                                peerClient.register(peerVO.getHost(), peerVO.getMissingServiceHosts());
                            if(!running.get()) break;
                            if(peerVO.getMissingSubscriptions() != null && !peerVO.getMissingSubscriptions().isEmpty())
                                peerClient.subscribe(peerVO.getHost(), peerVO.getMissingSubscriptions());
                        }
                    }
                } else if(firstRun) {
                    readSerialized();
                    firstRun = false;
                }
                
                // Save State and Sleep
                if(running.get()){
                    saveServiceHostState();
                    saveSubscriptionsState();
                    System.out.println("PeerSyncThread Sleeping...");
                    sleeping.set(true);
                    Thread.sleep(SLEEP_TIME);
                    sleeping.set(false);
                }
            } catch(InterruptedException e){
            } catch(Exception e){
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
            }
        }
        LOGGER.log(Level.WARNING, "PeerSyncThread Stopped.");
        System.out.println("PeerSyncThread Stopped.");
        
        sleeping.set(true);
        stop.set(true);
    }
}
